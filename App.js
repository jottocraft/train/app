/**
 *   ____      _ _             _         _     _
 *  / ___|__ _| | |_ _ __ __ _(_)_ __   | |   (_)_   _____
 * | |   / _` | | __| '__/ _` | | '_ \  | |   | \ \ / / _ \
 * | |__| (_| | | |_| | | (_| | | | | | | |___| |\ V /  __/
 *  \____\__,_|_|\__|_|  \__,_|_|_| |_| |_____|_| \_/ \___|
 * 
 *                _____
 *   ___  _ __   | ____|_  ___ __   ___
 *  / _ \| '_ \  |  _| \ \/ / '_ \ / _ \
 * | (_) | | | | | |___ >  <| |_) | (_) |
 *  \___/|_| |_| |_____/_/\_\ .__/ \___/
 *                          |_|
 */

import React from "react";
import { AppState, Platform } from 'react-native';

import * as SplashScreen from 'expo-splash-screen';
if (!__DEV__ && (AppState.currentState === "active")) SplashScreen.preventAutoHideAsync();

import { enableFreeze } from 'react-native-screens';
import * as Sentry from 'sentry-expo';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import "./utils/platform";

import { ThemeStore } from "./contexts/theme";
import { TimetableStore } from "./contexts/timetable";
import { RootPrefStore } from "./contexts/prefs";
import { ScreensStore } from './contexts/screens';
import Shell from "./Shell";

Sentry.init({
  dsn: "https://f93582ecf1e543d19c8e96167a4df210@o226991.ingest.sentry.io/6577103",
  enableInExpoDevelopment: false,
  tracesSampleRate: 0.33
});

enableFreeze(true);

export default function App() {
  global.appMountedAt = new Date().getTime();

  const innerJSX = Platform.OS === "ios" ? (
    <SafeAreaProvider>
      <ScreensStore>
        <Shell />
      </ScreensStore>
    </SafeAreaProvider>
  ) : (
    <ScreensStore>
      <Shell />
    </ScreensStore>
  );

  return (
    <RootPrefStore>
      <TimetableStore>
        <ThemeStore>
          {innerJSX}
        </ThemeStore>
      </TimetableStore>
    </RootPrefStore>
  );
};