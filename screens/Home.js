import React, { useCallback, useContext, useState } from 'react';
import {
    Platform,
    RefreshControl
} from 'react-native';
import Screen from '../components/Screen';

import themeContext from '../contexts/theme';
import { alertContext } from '../contexts/timetable';

import usePref from '../contexts/prefs';
import HomeHeader from '../components/HomeHeader';
import Tracks from '../components/Tracks';

export default function HomeScreen() {
    const selectedStream = usePref("selectedStream");

    const theme = useContext(themeContext);
    const alerts = useContext(alertContext);

    const [refreshing, setRefreshing] = useState(false);
    const onRefresh = useCallback(() => {
        setRefreshing(true);
        alerts.refresh().then(() => setRefreshing(false));
    }, [alerts?.refresh]);

    return (
        <Screen
            header={{ title: "Trains", icon: "train" }}
            fullWidth
            refreshControl={
                (selectedStream.val === "real") && alerts?.refresh ? <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                    colors={[theme.accent.type]}
                    progressBackgroundColor={theme.dark ? theme.ui[10] : theme.ui[0]}
                    tintColor={Platform.OS === "ios" ? theme.type[10] : theme.accent.type}
                /> : null
            }>

            <HomeHeader />

            <Tracks />
        </Screen>
    );
};