import React, { useState, useContext, useMemo, useCallback } from 'react';
import {
  Platform,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  Modal,
  TouchableWithoutFeedback,
  useColorScheme
} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import { useNavigation } from '@react-navigation/native';

import RoundIcon from '../components/RoundIcon';

import Screen, { useAsideRoute } from '../components/Screen';

import { streamContext, timetableContext } from '../contexts/timetable';
import themeContext from '../contexts/theme';

import { getService } from '../utils/initTimetable';
import { useTimeFormatter } from "../utils/time";
import useTrainState from '../utils/trainState';
import usePref from '../contexts/prefs';
import TouchEffect from '../components/TouchEffect';
import EmptyFeedback from '../components/EmptyFeedback';
import Button from '../components/Button';

function isToday(date) {
  var today = new Date();
  var d1 = new Date(date);

  return d1.getFullYear() === today.getFullYear() &&
    d1.getMonth() === today.getMonth() &&
    d1.getDate() === today.getDate();
}

function minutesBetween(date1, date2) {
  var diff = Math.abs(date1.getTime() - date2.getTime());
  return Math.round(diff / 60000);
}

function formatYyyyMmDd(date) {
  const monthString = String(date.getMonth() + 1);
  const dayString = String(date.getDate());
  return `${date.getFullYear()}-${monthString.length === 1 ? "0" + monthString : monthString}-${dayString.length === 1 ? "0" + dayString : dayString}`;
}

function DatePickerModalWrapper({ children, selectedDate, onChange }) {
  const theme = useContext(themeContext);

  return (
    <Modal
      visible={true}
      transparent={true}
    >
      <View style={{ position: "relative", flex: 1, backgroundColor: "#00000088", justifyContent: "center", alignItems: "center" }}>
        <View style={{ position: "absolute", width: "100%", height: "100%", zIndex: 10 }}>
          <TouchableWithoutFeedback onPress={() => onChange()}>
            <View style={{ width: "100%", height: "100%", flex: 1 }} />
          </TouchableWithoutFeedback>
        </View>
        <View style={{
          position: "relative",
          zIndex: 20,
          width: "100%",
          height: "100%",
          maxWidth: 400,
          maxHeight: 500,
          backgroundColor: theme.dark ? theme.ui[10] : theme.ui[0],
          borderRadius: 8,

          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 5,
          },
          shadowOpacity: 0.34,
          shadowRadius: 6.27,
          elevation: 10
        }}>
          {children}
          <View style={{ flexDirection: "row", margin: 10 }}>
            <View style={{ flex: 1 }}>
              <Button onPress={() => onChange()}>Cancel</Button>
            </View>
            <View style={{ width: 10 }} />
            <View style={{ flex: 1 }}>
              <Button onPress={() => onChange(null, selectedDate)}>Select</Button>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
}

/* On Android, a DateTimePicker display=default opens immediately; on iOS,
 * it adds a second button that you need to press in order to get the
 * picker.  We wrap the DateTimePicker in a Modal on iOS -- suggested here:
 *
 *  https://github.com/react-native-datetimepicker/datetimepicker/issues/483#issuecomment-1682427503
 *
 * to get a modal picker immediately, and use the default behavior on
 * Android otherwise.
 */
function DatePickerImmediate({ minimumDate, value, onChange }) {
  if (Platform.OS === 'android') {
    return (
      <DateTimePicker
        minimumDate={minimumDate}
        value={value}
        onChange={onChange}
        display="default"
      />
    );
  }

  if (Platform.OS === "ios") {
    const theme = useContext(themeContext);
    const [selectedDate, setSelectedDate] = useState(undefined);

    return (
      <DatePickerModalWrapper selectedDate={selectedDate} onChange={onChange}>
        <DateTimePicker
          minimumDate={minimumDate}
          value={value}
          onChange={(e, d) => setSelectedDate(d)}
          display="inline"
          style={{ flex: 1 }}
          accentColor={theme.accent.type}
          themeVariant={theme.dark ? "dark" : "light"}
        />
      </DatePickerModalWrapper>
    );
  }

  if (Platform.OS === "web") {
    const [selectedDate, setSelectedDate] = useState(undefined);

    return (
      <DatePickerModalWrapper selectedDate={selectedDate} onChange={onChange}>
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
          <input
            type="date"
            min={minimumDate ? formatYyyyMmDd(minimumDate) : undefined}
            defaultValue={value ? formatYyyyMmDd(value) : undefined}
            onChange={e => setSelectedDate(new Date(Number(e.target.value.slice(0, 4)), Number(e.target.value.slice(5, 7)) - 1, Number(e.target.value.slice(8, 10))))}
          />
        </View>
      </DatePickerModalWrapper>
    );
  }
}

`minimumDate={dev.val ? undefined : new Date()}
            value={date.date}
            onChange={(e, d) => {
              if (d) {
                setDate({ date: d, pick: false });
              } else {
                setDate({ ...date, pick: false });
              }
            }}`

function PickerButton({ track, prompt, label, val, callback }) {
  const theme = useContext(themeContext);
  const debugLayout = usePref("debugLayout");
  const { navigate } = useNavigation();

  return (
    <View style={{ marginBottom: theme.bigScreen ? 22 : 0 }}>
      <Text style={[{ color: theme.type[10], textAlignVertical: "center", marginBottom: 5, backgroundColor: debugLayout.val ? "green" : null, height: 24, fontFamily: theme.font.regular }]}>{label}</Text>
      <TouchEffect
        outerStyle={{
          width: "100%",
          height: 46,
          borderRadius: 10,
          borderWidth: 1,
          borderColor: theme.dark ? theme.ui[20] : theme.ui[30]
        }}
        innerStyle={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          paddingHorizontal: 10
        }}

        onPress={() => navigate("Picker", { title: prompt, track, callback })}>
        <Text style={{ color: theme.type[30], fontFamily: theme.font.regular }}>{val}</Text>
        <RoundIcon color={theme.type[40]} name="arrow_drop_down" size={22} />
      </TouchEffect>
    </View>
  );
}

export default function TimetableScreen() {
  const { navigate } = useNavigation();
  const asideRoute = useAsideRoute();

  const theme = useContext(themeContext);

  const timetableOrigin = usePref("timetableOrigin");
  const timetableDest = usePref("timetableDest");
  const debugLayout = usePref("debugLayout");

  const formatTime = useTimeFormatter();

  const stream = useContext(streamContext);

  const timetable = useContext(timetableContext);

  const track = "super";

  const stops = timetable.tracks[track];
  const trips = timetable.trips;
  const service = timetable.service;

  const [date, setDate] = useState({ date: new Date(), pick: false });

  const selectedToday = isToday(date.date);
  const future = selectedToday ? false : date.date.toISOString();

  const tripOptions = useMemo(() => {
    const serviceDate = getService(service, date.date);

    return Object.keys(trips).map(k => {
      const train = trips[k];

      if (serviceDate.includes(train.service)) {
        //train in service
        let originStopTime = null;
        let destStopTime = null;

        for (const stopTime of train.stops) {
          const stop = stops.find(s => s.IDs.includes(stopTime.id));
          if (stop.name == timetableOrigin.val) originStopTime = stopTime[selectedToday ? stream.id : "scheduled"].dep;
          if (stop.name == timetableDest.val) destStopTime = stopTime[selectedToday ? stream.id : "scheduled"].arr;
        }

        if (originStopTime && destStopTime && (originStopTime < destStopTime)) {
          //train stops here
          return { originStopTime, destStopTime, train };
        }
      }

      return null;
    }).filter(t => t);
  }, [timetable, selectedToday, date, timetableOrigin, timetableDest]);

  const trainStates = useTrainState({ future }, ...tripOptions.map(t => t.train));
  const dev = usePref("dev");

  const swapEndpoints = useCallback(() => {
    const newOrigin = timetableDest.val, newDest = timetableOrigin.val;
    timetableOrigin.update(newOrigin);
    timetableDest.update(newDest);
  }, [timetableOrigin, timetableDest]);

  const pressSetDate = useCallback(() => {
    return setDate({ ...date, pick: true });
  }, [date, setDate]);

  let hasRenderedHeader = false;

  return (
    <Screen header={{ title: "Trip Planner", description: selectedToday ? "Showing trains for today" : "Showing trains for " + date.date.toDateString(), icon: "rebase_edit" }} fullWidth>
      <View style={{ paddingHorizontal: theme.screenHorizPadding }}>

        <View style={{ flexDirection: theme.bigScreen ? "row" : "column", alignItems: theme.bigScreen ? "center" : null }}>
          <View style={{ flex: theme.bigScreen ? 1 : null }}>
            <PickerButton track={track} callback={selection => timetableOrigin.update(selection)} label="Origin" prompt="Set origin" val={timetableOrigin.val} />
          </View>

          {!theme.bigScreen ? (
            <View style={{ flexDirection: "row", justifyContent: "flex-end", zIndex: 10 }}>
              <TouchableOpacity onPress={pressSetDate} style={{ padding: 12, backgroundColor: debugLayout.val ? "blue" : null }}>
                <RoundIcon name={"date_range"} color={selectedToday ? theme.type[20] : theme.accent.type} size={28} />
              </TouchableOpacity>

              <TouchableOpacity onPress={swapEndpoints} style={{ padding: 12, backgroundColor: debugLayout.val ? "blue" : null }}>
                <RoundIcon name={"compare_arrows"} color={theme.type[20]} size={28} />
              </TouchableOpacity>
            </View>
          ) : <View style={{ width: 20 }} />}

          <View style={{ marginTop: theme.bigScreen ? 0 : -30, flex: theme.bigScreen ? 1 : null }}>
            <PickerButton track={track} callback={selection => timetableDest.update(selection)} label="Destination" prompt="Set destination" val={timetableDest.val} />
          </View>
        </View>

        {theme.bigScreen ? (
          <View style={{ flexDirection: "row", justifyContent: "flex-end", alignItems: "center", marginBottom: 20 }}>
            <View style={{ flexDirection: "row" }}>
              <View style={{ marginRight: 5 }}>
                <Button onPress={pressSetDate} icon="date_range">{selectedToday ? "Today" : date.date.toDateString()}</Button>
              </View>
              <View style={{ marginLeft: 5 }}>
                <Button onPress={swapEndpoints} icon="compare_arrows">Swap</Button>
              </View>
            </View>
          </View>
        ) : null}

        {date.pick ? (
          <DatePickerImmediate
            minimumDate={dev.val ? undefined : new Date()}
            value={date.date}
            onChange={(e, d) => {
              if (d) {
                setDate({ date: d, pick: false });
              } else {
                setDate({ ...date, pick: false });
              }
            }} />
        ) : null}
      </View>

      <View style={{ marginTop: 20 }}>
        {
          timetable.expired ? (
            <EmptyFeedback title="Timetable expired" icon="timer_off">
              The timetable version installed on your device is out-of-date and has expired. Please enable and connect to realtime data and/or update Caltrain Live to get the latest schedule.
            </EmptyFeedback>
          ) : tripOptions.length ? (
            tripOptions.sort((a, b) => {
              const keyA = new Date(a.originStopTime).getTime(),
                keyB = new Date(b.originStopTime).getTime();

              if (selectedToday) {
                // If today is the selected date, seperate upcoming departures
                var now = new Date().getTime();
                if ((keyA > now) && (keyB < now)) return -1;
                if ((keyB > now) && (keyA < now)) return 1;
              }

              if (keyA < keyB) return -1;
              if (keyA > keyB) return 1;
              return 0;
            }).map((data, i) => {
              if (i == 0) {
                hasRenderedHeader = false;
              }

              var header;
              if (selectedToday && !hasRenderedHeader && (new Date() > new Date(data.originStopTime))) {
                hasRenderedHeader = true;
                header = (
                  <View style={{ marginTop: 40, marginBottom: 5, flexDirection: "row", alignItems: "center", paddingHorizontal: theme.screenHorizPadding }}>
                    <View style={{ marginRight: 5 }}><RoundIcon name={"history"} color={theme.type[10]} size={26} /></View>
                    <Text style={{ fontFamily: theme.font.semibold, fontSize: 20, color: theme.type[10] }}>Previous Departures</Text>
                  </View>
                );
              }

              const nextStop = trainStates.find(s => s.id == data.train.id);
              const active = asideRoute?.name === "Train" && asideRoute?.params?.id === data.train.id;

              return (
                <View key={data.train.id}>
                  {header}
                  <TouchEffect wide disabled={active} onPress={() => { navigate("Train", { id: data.train.id, future }) }}
                    outerStyle={{
                      backgroundColor: active ? theme.ui[10] : undefined,
                      opacity: selectedToday && (new Date() > new Date(data.originStopTime)) ? .4 : 1
                    }}
                    innerStyle={{
                      paddingVertical: 12, flexDirection: 'row', justifyContent: 'space-between', alignItems: "center"
                    }}>
                    <View>
                      <Text style={{ fontFamily: theme.font.semibold, fontSize: 14, color: theme.type[20] }}>{data.train.name}</Text>
                      <Text style={{ fontFamily: theme.font.medium, fontSize: 12, color: theme.lineType[data.train.line] }}>{data.train.lineType || service.regular.lineNames[data.train.line].long}</Text>
                    </View>
                    <View style={{ alignItems: "flex-end" }}>
                      <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <Text style={{ fontFamily: theme.font.bold, fontSize: 12, color: theme.type[20], marginRight: 5 }}>{formatTime(data.originStopTime) + " - " + formatTime(data.destStopTime)}</Text>
                        <Text style={{ fontFamily: theme.font.medium, fontSize: 12, color: theme.type[50], textAlign: "right" }}>{"(" + minutesBetween(data.destStopTime, data.originStopTime) + " min)"}</Text>
                      </View>
                      {nextStop?.time ? (
                        <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-end", marginTop: 2 }}>
                          {nextStop.stop ? (
                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                              <View style={{ marginRight: 2 }}><RoundIcon name={"flag"} color={theme.type[30]} size={14} /></View>
                              <Text style={{ fontFamily: theme.font.medium, fontSize: 10, color: theme.type[30], textAlign: "right" }}>{nextStop.stop.name}</Text>
                            </View>
                          ) : null}
                          {nextStop.delay ? (
                            <View style={{ flexDirection: "row", alignItems: "center", marginLeft: 10 }}>
                              <View style={{ marginRight: 2 }}><RoundIcon name={nextStop.icon} color={nextStop.color} size={14} /></View>
                              <Text style={{ fontFamily: theme.font.medium, fontSize: 10, color: nextStop.color, textAlign: "right" }}>{nextStop.delay}</Text>
                            </View>
                          ) : null}
                        </View>
                      ) : null}
                    </View>
                  </TouchEffect>
                </View>
              )
            })
          ) : (
            <EmptyFeedback title="No trips">
              Caltrain Live couldn't find any direct trips from {timetableOrigin.val} to {timetableDest.val} {selectedToday ? "today" : `on ${date.date.toDateString()}`}.
              Try changing your trip parameters. You may need to take multiple trains.
            </EmptyFeedback>
          )
        }
      </View>
    </Screen>
  );
};