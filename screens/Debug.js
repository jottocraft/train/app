import React, { useContext, useState } from 'react';
import {
    Platform,
    Text,
    TouchableOpacity,
    View,
    ScrollView,
    Linking,
    Image
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as Updates from "expo-updates";

import Screen from '../components/Screen';
import Button from '../components/Button';

import themeContext from '../contexts/theme';
import usePref from '../contexts/prefs';
import { timetableContext } from '../contexts/timetable';

export default function DebugScreen({ navigation }) {
    const theme = useContext(themeContext);
    const timetable = useContext(timetableContext);

    const readAlerts = usePref("readAlerts");
    const [otaStatus, setOtaStatus] = useState("Check for OTA updates");

    return (
        <Screen header={{ title: "Debug", icon: "bug_report", description: "* = restart required" }}>
            <View style={{ marginVertical: 5 }}>
                <Button onPress={() => { readAlerts.update([]); }}>Reset read alerts</Button>
            </View>
            <View style={{ marginVertical: 5 }}>
                <Button onPress={() => { AsyncStorage.removeItem("timetable"); }}>Remove downloaded timetable*</Button>
            </View>
            <View style={{ marginVertical: 5 }}>
                <Button onPress={() => { delete timetable.serviceBaseMs; }}>Reinitialize timetable on next update</Button>
            </View>
            <View style={{ marginVertical: 5 }}>
                <Button onPress={async () => {
                    try {
                        const update = await Updates.checkForUpdateAsync();
                        console.log("Caltrain Live debug update manifest", update.manifest);
                        if (update.isAvailable) {
                            setOtaStatus("Downloading OTA update " + update.manifest.metadata.otaVersion);
                            await Updates.fetchUpdateAsync();
                            navigation.navigate("SettingsStack", { screen: "OTAUpdate" });
                        } else {
                            setOtaStatus("No OTA update available");
                        }
                    } catch(e) {
                        console.error("Caltrain Live debug update error", e);
                        setOtaStatus("Update error. Check logcat.");
                    }
                }}>{otaStatus}</Button>
            </View>
            <View style={{ marginVertical: 5 }}>
                <Button onPress={() => { Updates.reloadAsync(); }}>Force reload</Button>
            </View>
            <View style={{ marginVertical: 5 }}>
                <Button onPress={() => { navigation.navigate("Settings"); }}>Go to Settings</Button>
            </View>
            <View style={{ marginVertical: 5 }}>
                <Button onPress={() => { navigation.navigate("SettingsStack", { screen: "OTAUpdate" }); }}>Go to OTAUpdateScreen</Button>
            </View>
        </Screen>
    );
}