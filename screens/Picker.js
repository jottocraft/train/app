import React, { useContext } from 'react';
import {
    Platform,
    Text,
    TouchableOpacity,
    View,
    ScrollView,
    Linking,
    Image
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Screen from '../components/Screen';
import Button from "../components/Button";
import themeContext from '../contexts/theme';

import { timetableContext } from '../contexts/timetable';
import TouchEffect from '../components/TouchEffect';

export default function PickerScreen({ route, callback, navigation }) {
    const theme = useContext(themeContext);
    const timetable = useContext(timetableContext);

    if (!route?.params?.title || !route?.params?.track) throw new Error("Navigated to PickerScreen without a title and/or track");

    const stops = timetable.tracks[route.params.track];

    function callback(name) {
        route.params.callback?.(name);
        navigation.goBack();
    }

    return (
        <Screen fullWidth header={{ title: route.params.title, icon: "flag" }}>
            <View style={{ marginTop: 20 }}>
                {stops && stops.map(stop => (
                    <View key={stop.name}>
                        <TouchEffect wide onPress={() => callback(stop.name)} innerStyle={{ paddingVertical: 16 }}>
                            <Text style={{ fontFamily: theme.font.medium, color: theme.type[20] }}>{stop.name}</Text>
                        </TouchEffect>
                    </View>
                ))}
            </View>
        </Screen>
    );
};