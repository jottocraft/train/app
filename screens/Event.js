import React from 'react';
import {
    Platform,
    Text,
    TouchableOpacity,
    View,
    ScrollView,
    Linking,
    Image
} from 'react-native';
import Screen from "../components/Screen";
import Alert from "../components/Alert";

export default function EventScreen({ route }) {
    return (
        <Screen header={{ title: route?.params?.title || "Event", description: route?.params?.description, icon: route?.params?.icon || "report" }}>
            <View style={{ marginTop: 10 }}>
                {route?.params?.alert ? <Alert {...route?.params?.alert} /> : null}
            </View>
        </Screen>
    );
};