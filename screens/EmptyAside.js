import React, { useContext } from 'react';
import { View } from 'react-native';

import themeContext from '../contexts/theme';
import RoundIcon from '../components/RoundIcon';

export default function EmptyAsideScreen() {
    const theme = useContext(themeContext);

    return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
            <RoundIcon name="rebase_edit" size={128} color={theme.ui[20]} />
        </View>
    );
};