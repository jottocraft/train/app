import React, { useContext, useState, useLayoutEffect, useMemo, useRef, useEffect } from 'react';
import {
    Platform,
    Text,
    TouchableOpacity,
    View,
    ScrollView,
    Image
} from 'react-native';
import Animated, { Easing, useSharedValue, withTiming, useAnimatedStyle, withRepeat } from 'react-native-reanimated';

import RoundIcon from '../components/RoundIcon';

import Alert from '../components/Alert';
import Screen from '../components/Screen';
import Button from '../components/Button';

import themeContext from '../contexts/theme';
import { alertContext, streamContext, timetableContext } from '../contexts/timetable';

import { formatDelay, useTimeFormatter } from '../utils/time';

import usePref from '../contexts/prefs';
import TouchEffect from '../components/TouchEffect';
import { useBlinkAnimation } from '../utils/useBlinkAnimation';

import stationImages from "../assets/images/optimized/stations";
import imageCredits from "../data/image-cc.json";
import EmptyFeedback from '../components/EmptyFeedback';

function Marquee({ children }) {
    const theme = useContext(themeContext);

    const [textWidth, setTextWidth] = useState(null);
    const [containerWidth, setContainerWidth] = useState(null);

    const scroll = useSharedValue(0);

    useEffect(() => {
        scroll.value = withRepeat(withTiming(textWidth / 2, { duration: textWidth * 4, easing: Easing.linear }), -1, false);
        return () => scroll.value = 0;
    }, [textWidth]);

    const animatedTransform = useAnimatedStyle(() => {
        return {
            transform: [
                { translateX: -scroll.value }
            ]
        }
    });

    const textStyle = { height: 24, color: theme.signType, fontFamily: theme.font.monoBold, fontSize: 14, textAlignVertical: "center", minWidth: containerWidth };

    return (
        <View onLayout={e => setContainerWidth(e.nativeEvent.layout.width)} style={{ width: "100%", height: 24 }}>
            <ScrollView horizontal onContentSizeChange={w => setTextWidth(w)} scrollEnabled={false}>
                <Animated.View style={[animatedTransform, { flexDirection: "row" }]}>
                    <Text style={textStyle}>{children}</Text>
                    <Text style={textStyle}>{children}</Text>
                </Animated.View>
            </ScrollView>
        </View>
    );
}

export default function StationScreen({ route, navigation }) {
    const { navigate } = navigation;

    const showEquipmentImages = usePref("showEquipmentImages");
    const timetableOrigin = usePref("timetableOrigin");
    const timetableDest = usePref("timetableDest");

    const formatTime = useTimeFormatter();

    const theme = useContext(themeContext);

    const alerts = useContext(alertContext);
    const stream = useContext(streamContext);

    const timetable = useContext(timetableContext);
    const trips = timetable.trips;
    const service = timetable.service;

    const stops = timetable.tracks[route?.params?.track ?? "ERROR_NO_TRACK"];
    if (!stops) throw "Navigated to StationScreen with an invalid track ID";

    const stop = stops[route?.params?.i ?? "ERROR_NO_STOP_INDEX"];
    if (!stop) throw "Navigated to StationScreen with an invalid stop ID";

    const stopNames = [stop.name, ...stop.aliases, ...stop.IDs];

    const [date, setDate] = useState(new Date());

    useEffect(() => {
        // Update the station screen every 5s
        function updateDate() {
            setDate(new Date());
        }
        const interval = setInterval(updateDate, 5 * 1000);
        return () => clearInterval(interval);
    }, []);

    //Sort and filter trains by the time that they stop here
    //Format: CLOSEST SB | SB | SB | CLOSEST NB | NB | NB
    const stopTrains = useMemo(() => {
        const count = { NB: 0, SB: 0 };

        return Object.keys(trips)
            .map(t => trips[t].inService && { train: t, stopIndex: trips[t].stops.findIndex(s => stop.IDs.includes(s.id)) })
            .filter(e => e && (e.stopIndex !== -1) && (e.stopIndex !== (trips[e.train].stops.length - 1)) && (trips[e.train].stops[e.stopIndex][stream.id].dep > date))
            .sort((a, b) => {
                return (
                    trips[a.train].stops[a.stopIndex][stream.id].dep
                ) - (
                        trips[b.train].stops[b.stopIndex][stream.id].dep
                    );
            })
            .filter(e => {
                count[trips[e.train].dir]++;
                return count[trips[e.train].dir] <= 3;
            })
            .sort((a, b) => {
                const c = trips[a.train].dir == "NB" ? 1 : 2;
                const d = trips[b.train].dir == "NB" ? 1 : 2;

                return d - c;
            });
    }, [trips, stream?.id, stop, date]);

    const stopLineTags = useMemo(() => {
        var data = [];

        Object.keys(trips).forEach(t => {
            var train = trips[t];
            if (!train.stops.find(s => stop.IDs.includes(s.id))) return;

            if (!data.find(d => d.id == train.line)) data.push({
                text: service.regular.lineNames[train.line].short,
                bg: theme.lineUI[train.line],
                color: theme.lineUIText[train.line],
                id: train.line
            });
        });

        return data;
    }, [stop, service, theme]);

    const blinkAnimation = useBlinkAnimation();

    const signage = useMemo(() => {
        if (!alerts.service) return;
        console.log("update signage memo");
        return alerts.service.filter(a => ((a.from == "PADS") && (!a.for || stopNames.find(name => a.for.includes(name))))).map(a => a.text).join("  ").trim();
    }, [alerts.service, stopNames]);

    const stationAlerts = useMemo(() => {
        if (!alerts.service) return;
        return alerts.service.map((alert, key) => {
            let match = false;

            if (alert.title && stopNames.find(a => alert.title.includes(a))) match = true;
            if (alert.text && stopNames.find(a => alert.text.includes(a))) match = true;
            if (alert.for && stopNames.find(a => alert.for.includes(a))) match = true;

            if (match) {
                return (
                    <View key={key} style={{ marginVertical: 5, paddingHorizontal: theme.screenHorizPadding }}>
                        <Alert {...alert} />
                    </View>
                );
            } else {
                return;
            }
        })
    }, [stop, alerts.service, stopNames]);

    const trainSigns = useMemo(() => {
        if (!stopTrains.length) {
            return (
                <EmptyFeedback title="No upcoming trains">
                    There aren't any trains that will stop at this station for the rest of the day.
                </EmptyFeedback>
            )
        }

        const signRendered = { NB: false, SB: false };
        return (
            <View style={{ marginVertical: 60 }}>
                {stopTrains.map((e) => {
                    const train = trips[e.train];
                    const trainStop = trips[e.train].stops[e.stopIndex];
                    const stopTime = trainStop[stream.id];

                    let dirHeader;
                    if (!signRendered[train.dir]) {
                        dirHeader = (
                            <View style={{ marginTop: signRendered.SB && (train.dir == "NB") ? 48 : 0, paddingHorizontal: theme.screenHorizPadding }}>
                                <View style={{ flex: 1, flexDirection: "row", alignItems: "center", justifyContent: "flex-start" }}>
                                    <View style={{ marginRight: 6, transform: train.dir === "SB" ? [{ rotate: "180deg" }] : [] }}>
                                        <RoundIcon name="navigation" color={theme.type[10]} size={22} />
                                    </View>
                                    <Text style={{ fontFamily: theme.font.bold, fontSize: 16, color: theme.type[10] }}>{train.dir == "NB" ? "Northbound" : "Southbound"} Departures</Text>
                                </View>
                                <View style={{ flexDirection: "row", justifyContent: "space-between", paddingVertical: 8, marginTop: 12 }}>
                                    <Text style={{ color: theme.type[30], fontFamily: theme.font.semibold, fontSize: 12, width: 80 }}>Train</Text>
                                    <Text style={{ color: theme.type[30], fontFamily: theme.font.semibold, fontSize: 12 }}>Scheduled time</Text>
                                    <Text style={{ color: theme.type[30], fontFamily: theme.font.semibold, fontSize: 12, flexGrow: 1, textAlign: "right" }}>Status</Text>
                                </View>
                            </View>
                        );
                        signRendered[train.dir] = true;
                    }

                    const trainStatus = stream.hasData && (stream.id !== "scheduled") ? (!train.online ? (date.getTime() < train.stops[0][stream.id].dep ? "" : "No data") : formatDelay(stopTime.dep - trainStop.scheduled.dep)) : stream.text;

                    return (
                        <View key={train.id}>
                            {dirHeader}
                            <TouchEffect wide onPress={() => { navigate('Train', { id: train.id }) }} innerStyle={{ flexDirection: "row", justifyContent: "space-between", paddingVertical: 16 }}>
                                <Text style={{ color: theme.signType, fontFamily: theme.font.monoBold, fontSize: 14, width: 80 }}>{"#" + train.name}</Text>
                                <Text style={{ color: theme.signType, fontFamily: theme.font.monoBold, fontSize: 14 }}>{formatTime(trainStop.scheduled.dep)}</Text>
                                {stopTime.arr && (date.getTime() >= stopTime.arr) && stopTime.dep && (Math.abs(date.getTime() - stopTime.dep) <= (5 * 60000)) ? (
                                    <Animated.Text style={[blinkAnimation, { color: theme.signType, fontFamily: theme.font.monoBold, fontSize: 14, flex: 1, textAlign: "right" }]}>ARRIVING</Animated.Text>
                                ) : (
                                    <Text style={{ color: theme.signType, fontFamily: theme.font.monoBold, fontSize: 14, flexGrow: 1, textAlign: "right" }}>
                                        {trainStatus?.toUpperCase()}
                                    </Text>
                                )}
                            </TouchEffect>
                        </View>
                    );
                })}
            </View>
        )
    }, [stopTrains, trips, theme, stream, formatTime, date]);

    const image = showEquipmentImages.val && imageCredits[stop.name] && stationImages[stop.name.toUpperCase().replaceAll(" ", "_")];

    return (
        <Screen fullWidth header={{
            title: stop.name,
            icon: "flag",
            image: image,
            imageCredits: imageCredits[stop.name],
            tags: stopLineTags
        }}>
            {signage ? (
                <Marquee>{signage}</Marquee>
            ) : null}

            {stationAlerts}

            {trainSigns}

            <View style={{ marginHorizontal: theme.screenHorizPadding }}>
                <View style={{ marginVertical: 5 }}>
                    <Button icon="arrow_forward" onPress={() => { timetableDest.update(stop.name); navigate("TimetableStack", { screen: "Timetable" }); }}>
                        {"Plan a trip to " + stop.name}
                    </Button>
                </View>

                <View style={{ marginVertical: 5 }}>
                    <Button icon="arrow_back" onPress={() => { timetableOrigin.update(stop.name); navigate("TimetableStack", { screen: "Timetable" }); }}>
                        {"Plan a trip from " + stop.name}
                    </Button>
                </View>
            </View>

        </Screen>
    );
};