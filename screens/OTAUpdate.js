import React, { useContext } from 'react';
import {
    Text,
    View,
} from 'react-native';
import * as Updates from "expo-updates";

import Screen from '../components/Screen';
import Button from "../components/Button";
import themeContext from '../contexts/theme';
import RoundIcon from '../components/RoundIcon';
import { getVersion } from '../utils/version';
import EmptyFeedback from '../components/EmptyFeedback';

export default function OTAUpdateScreen() {
    const theme = useContext(themeContext);
    const { availableUpdate, isUpdatePending } = Updates.useUpdates();

    const metadata = availableUpdate?.manifest?.metadata;

    return (
        <Screen header={{ title: metadata?.critical ? "Critical update" : "Update Caltrain Live", icon: metadata?.critical ? "priority_high" : "system_update" }}>
            {metadata && isUpdatePending ? (
                <>
                    <Text style={{ fontFamily: theme.font.medium, fontSize: 14, color: theme.type[40], textAlign: "center" }}>{metadata.critical ? (
                        "A critical update has been released for Caltrain Live. Please tap the button below to install this update before proceeding."
                    ) : (
                        "An update is available for Caltrain Live. It'll automatically install next time you open the app, or you can choose to install it now."
                    )}</Text>
                    <View style={{ padding: 20, borderRadius: 10, backgroundColor: theme.ui[5], marginTop: 20 }}>
                        <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between" }}>
                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                                <RoundIcon name="history" size={20} color={theme.type[20]} />
                                <Text style={{ fontFamily: theme.font.medium, fontSize: 16, color: theme.type[20], marginLeft: 5 }}>Changelog</Text>
                            </View>
                            <Text style={{ fontFamily: theme.font.regular, fontSize: 12, color: theme.type[40] }}>Version {getVersion({ OTA: metadata.otaVersion }).name}</Text>
                        </View>
                        {metadata.notes ? (
                            <View style={{ marginTop: 10 }}>
                                <Text style={{ fontFamily: theme.font.regular, fontSize: 14, color: theme.type[20] }}>{metadata.notes}</Text>
                            </View>
                        ) : null}
                        <View style={{ marginTop: 20 }}>
                            {metadata.changelog.map(change => (
                                <View style={{ marginTop: 10, flexDirection: "row" }}>
                                    <Text style={{ fontFamily: theme.font.regular, fontSize: 14, color: theme.type[20], marginRight: 5 }}>-</Text>
                                    <Text style={{ fontFamily: theme.font.regular, fontSize: 14, color: theme.type[20] }}>{change.summary}</Text>
                                </View>
                            ))}
                        </View>
                    </View>
                    <View style={{ marginTop: 20 }}>
                        <Button onPress={() => Updates.reloadAsync()}>Install and reload</Button>
                    </View>
                </>
            ) : metadata ? (
                <EmptyFeedback title="Downloading update..." icon="download">
                    Caltrain Live is downloading a new update. It'll show up here when it's ready for you.
                </EmptyFeedback>
            ) : (
                <EmptyFeedback title="You're up to date" icon="done">
                    You're already using the latest version of Caltrain Live.
                </EmptyFeedback>
            )}
        </Screen>
    );
};