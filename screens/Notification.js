import React, { useContext } from 'react';
import {
    Platform,
    Text,
    TouchableOpacity,
    View,
    ScrollView,
    Linking,
    Image
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Screen from '../components/Screen';
import Button from "../components/Button";
import themeContext from '../contexts/theme';

export default function NotificationScreen({ navigation, route }) {
    const theme = useContext(themeContext);

    return (
        <Screen header={{ title: route?.params?.title ?? "Notification", icon: route?.params?.icon ?? "notification" }}>
            <Text style={{ fontFamily: theme.font.medium, fontSize: 14, color: theme.type[40], textAlign: "center" }}>{route?.params?.description ?? "ERROR [2]"}</Text>
            <View style={{ marginTop: 20 }}>
                <Button onPress={() => navigation.goBack()}>OK</Button>
            </View>
        </Screen>
    );
};