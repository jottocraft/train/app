import React, { useContext, useMemo, useState } from 'react';
import {
    Text,
    View,
    Image,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Linking,
    Platform
} from 'react-native';
import { useUpdates } from 'expo-updates';
import { BUILTIN_THEMES } from "shamrock-ux/lib/core/themes";

import { timetableContext } from '../contexts/timetable';
import themeContext from '../contexts/theme';
import usePref from '../contexts/prefs';

import RoundIcon from '../components/RoundIcon';
import Screen from '../components/Screen';
import HeaderSection from '../components/HeaderSection';
import Setting from '../components/Setting';
import { useNavigation } from '@react-navigation/native';
import { getVersion } from '../utils/version';


//Make sure to also update prefs in Globals.js

function getSettings(dev, theme) {
    const settings = {
        display: {
            title: "Display",
            icon: "design_services",
            prefs: {
                theme: {
                    title: "Theme",
                    desc: "Sets the color theme used across Caltrain Live.",
                    opts: [
                        { val: "system", text: "System default", icon: theme.bigScreen ? "tablet" : "smartphone" },
                        ...Object.keys(BUILTIN_THEMES)
                            .filter(id => !BUILTIN_THEMES[id].hidden)
                            .map(id => (
                                { val: id, text: id === "antique" ? "Paper" : BUILTIN_THEMES[id].name, icon: id === "antique" ? "article" : BUILTIN_THEMES[id].icon }
                            ))
                    ]
                },
                showSingleTracking: {
                    title: "Show single tracking icons",
                    desc: "Show construction icons on the tracks next to stations that are single tracking."
                },
                showEquipmentImages: {
                    title: "Station images",
                    desc: "Show station images in the station details screen."
                },
                showSeconds: {
                    title: "Show seconds",
                    desc: "Show seconds for timestamps throughout Caltrain Live. Enabling this will hide the AM/PM indicator."
                }
            }
        },
        stops: {
            title: "Stops",
            icon: "timer",
            prefs: {
                autoScrollToNextStop: {
                    title: "Automatically scroll to the next stop",
                    desc: "Automatically scroll to the next stop when opening train details.",
                },
                keepTimetableSFAtTop: {
                    title: "Sort by direction",
                    desc: "Sort train stops from north to south instead of first to last."
                },
                delayPerStop: {
                    title: "Individual stop delays",
                    desc: "Show the delay at each train stop if available."
                },
                showStopBypasses: {
                    title: "Show bypasses",
                    desc: "Show estimates for when trains will pass by unscheduled stops. Shown as " + (theme.dark ? "dark" : "light") + " italic text on the train stop list.",
                }
            }
        }
    };

    if ((Platform.OS === "android") && !theme.bigScreen) {
        settings.display.prefs.trivialBackButtons = {
            title: "Show in-app back buttons",
            desc: "Show back buttons in-app that can be used instead of Android's built-in back button or gesture."
        };
    }

    if (dev) {
        settings.debug = {
            title: "Debugging",
            icon: "bug_report",
            prefs: {
                debugLayout: {
                    title: "Debug Layouts",
                    desc: "Show boxes and indicators to debug layouts and alignment across Caltrain Live."
                },
                debugRealtimeError: {
                    title: "Force Realtime Error",
                    desc: "Causes the realtime function to throw an error for debugging."
                },
                debugUseArrBypass: {
                    title: "Use Arrival Times in the Bypass Calculation",
                    desc: "Uses arrival times for calculating the end of the bypass leg (if available)."
                },
                debugAllAlerts: {
                    title: "Show All Active Alerts",
                    desc: "Show all active alerts in the alerts screen with debugging information, regardless of their importance."
                },
                debugRealtimeDevelopment: {
                    title: "Use Development Realtime Server",
                    desc: "Requires a jottocraft intranet connection. Visit hq/as-realtime-dev for details."
                },
                debugDarkAdaptiveColor: {
                    title: "Use lighter adaptive colors",
                    desc: "Uses a lighter shade of the system adaptive color pallete in light mode on supported Android 12 devices."
                },
                debugForceAccentColor: {
                    title: "Force system accent color usage",
                    desc: "Always use the dynamic system accent color (API 31+) regardless of the set theme."
                },
                debugBigScreenUX: {
                    title: "Override big screen UX",
                    desc: "Sets the navigation experience to the opposite of what Caltrain Live detects."
                }
            }
        };
    }

    return settings;
}

function InfoTile({ title, icon, detail, children }) {
    const theme = useContext(themeContext);

    return (
        <View>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
                <RoundIcon name={icon} size={14} color={theme.type[40]} />
                <Text style={{ marginLeft: 5, color: theme.type[40], fontSize: 12, fontFamily: theme.font.regular }}>{title}</Text>
            </View>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ color: theme.type[40], fontSize: 12, fontFamily: theme.font.regular }}>{children}</Text>
                {detail ? <Text style={{ marginLeft: 10, color: theme.type[100], fontSize: 10, fontFamily: theme.font.monoBold }}>{detail}</Text> : null}
            </View>
        </View>
    );
}

export default function SettingsScreen() {
    const { navigate } = useNavigation();
    const dev = usePref("dev");

    const { availableUpdate, isUpdatePending } = useUpdates();

    const theme = useContext(themeContext);
    const timetable = useContext(timetableContext);
    const [settings, setSettings] = useState(getSettings(dev.val, theme));

    useMemo(() => {
        setSettings(getSettings(dev.val, theme));
    }, [dev, theme.dark]);

    return (
        <Screen header={{ title: "Settings", description: dev.val && "Debugging features enabled", icon: "settings" }} fullWidth>
            {availableUpdate && isUpdatePending ? (
                <View style={{ marginBottom: 10 }}>
                    <HeaderSection accented allTouchable onPress={() => navigate("OTAUpdate")} title="An update is available" icon="system_update" />
                </View>
            ) : null}

            <View style={{ flexDirection: theme.bigScreen ? "row" : "column", flexWrap: theme.bigScreen ? "wrap" : undefined }}>
                {
                    settings && Object.keys(settings).map((k, i) => {
                        var group = settings[k];

                        return (
                            <View style={{ marginTop: !theme.bigScreen && (i === 0) ? 24 : 48, flex: theme.bigScreen ? 1 : undefined, minWidth: theme.bigScreen ? 400 : undefined }} key={i}>
                                <View style={{ paddingHorizontal: theme.screenHorizPadding, marginBottom: 10, flexDirection: "row", alignItems: "center" }}>
                                    <View style={{ marginRight: 5 }}><RoundIcon name={group.icon} color={theme.type[0]} size={24} /></View>
                                    <Text style={{ fontFamily: theme.font.semibold, fontSize: 20, color: theme.type[0] }}>{group.title}</Text>
                                </View>

                                {
                                    Object.keys(group.prefs).map((k, kI) => {
                                        const pref = group.prefs[k];
                                        return <Setting key={k} name={k} pref={pref} />
                                    })
                                }
                            </View>
                        )
                    })
                }
            </View>

            <View style={{ marginTop: 64, marginHorizontal: theme.screenHorizPadding }}>
                <View style={{ marginTop: 20 }}>
                    <InfoTile
                        title="App experience"
                        icon={Platform.OS === "android" ? "android" : Platform.OS === "ios" ? "ios" : Platform.OS === "web" ? "language" : "code"}
                        detail={getVersion().number}
                    >
                        {getVersion().name}
                    </InfoTile>
                </View>
                <View style={{ marginTop: 20 }}>
                    <InfoTile title="Installed timetable" icon="schedule" detail={timetable.liveasset.type + "/" + timetable.liveasset.ver}>{timetable.liveasset.name}</InfoTile>
                </View>
            </View>

            <View style={{ marginBottom: 20 }}>
                <View style={{ height: 100 }}></View>
                <View style={{ alignItems: "center" }}>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <Image
                            style={{ width: 75, height: 30, marginRight: 10 }}
                            source={require('../assets/images/optimized/graphics/footer.png')}
                        />
                        <Text style={{ fontSize: 22, fontFamily: theme.font.medium, color: theme.type[0] }}>jottocraft</Text>
                    </View>
                    <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
                        <Text style={{ fontSize: 14, fontFamily: theme.font.regular, color: theme.type[20] }}>Copyright (c) jottocraft 2018-</Text>
                        <TouchableWithoutFeedback onLongPress={() => dev.toggle()}><Text style={{ fontSize: 14, fontFamily: theme.font.regular, color: theme.type[30] }}>2024</Text></TouchableWithoutFeedback>
                    </View>
                    <View>
                        <Text style={{ marginTop: 5, fontSize: 12, fontFamily: theme.font.regular, color: theme.type[40] }}>Caltrain Live is open source at jottocraft.com/oss</Text>
                    </View>
                </View>
            </View>
        </Screen>
    );
}