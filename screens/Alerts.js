import React, { useContext, useEffect } from 'react';
import {
    Platform,
    Text,
    TouchableOpacity,
    View,
    ScrollView,
    Linking,
    Image
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Screen from '../components/Screen';
import Alert, { isAlertImportant } from '../components/Alert';

import { alertContext } from '../contexts/timetable';
import themeContext from '../contexts/theme';

import RoundIcon from '../components/RoundIcon';
import usePref from '../contexts/prefs';
import EmptyFeedback from '../components/EmptyFeedback';

export default function AlertScreen() {
    const alerts = useContext(alertContext);
    const theme = useContext(themeContext);

    const readAlerts = usePref("readAlerts");
    const debugAllAlerts = usePref("debugAllAlerts");

    useEffect(() => {
        if (!alerts.service) return;

        var added = false;
        alerts.service.forEach(a => {
            if ((a.from === "511") && a.id && !readAlerts.val.includes(a.id)) {
                readAlerts.val.push(a.id);
                added = true;
            }
        });

        if (added) return () => readAlerts.update([...readAlerts.val]);
    }, []);

    const importantAlerts = alerts.service?.filter(a => isAlertImportant(a, readAlerts.val)) ?? [];
    const miscAlerts = alerts.service?.filter(a => !importantAlerts.includes(a) && (debugAllAlerts.val ? true : !a.for)) ?? [];

    return (
        <Screen header={{ title: "Alerts", icon: "campaign" }}>
            {importantAlerts.length || miscAlerts.length ? (
                <>
                    {!importantAlerts.length ? (
                        <View style={{ marginTop: 20, marginBottom: 0, flexDirection: "row", alignItems: "center" }}>
                            <View style={{ marginRight: 5 }}><RoundIcon name={"done"} color={theme.type[20]} size={22} /></View>
                            <Text style={{ fontFamily: theme.font.medium, fontSize: 16, color: theme.type[20] }}>No important alerts</Text>
                        </View>
                    ) : (
                        <View>
                            <View style={{ marginTop: 20, marginLeft: 6, marginBottom: 5, flexDirection: "row", alignItems: "center" }}>
                                <View style={{ marginRight: 4 }}><RoundIcon name={"priority_high"} color={theme.type[10]} size={26} /></View>
                                <Text style={{ fontFamily: theme.font.bold, fontSize: 20, color: theme.type[10] }}>Important Alerts</Text>
                            </View>
                            <View>
                                {importantAlerts.map((alert, i) => {
                                    return <View style={{ marginVertical: 5 }}><Alert {...alert} /></View>;
                                })}
                            </View>
                        </View>
                    )}
                    {miscAlerts.length ? (
                        <View>
                            <View style={{ flexGrow: 1, flexShrink: 1, height: 1, backgroundColor: theme.ui[30], borderRadius: 10, marginVertical: 22, marginHorizontal: 10 }}></View>
                            <View>
                                {miscAlerts.map((alert, i) => {
                                    return <View style={{ marginVertical: 5 }}><Alert {...alert} /></View>;
                                })}
                            </View>
                        </View>
                    ) : null}
                </>
            ) : (
                <EmptyFeedback title="No alerts">
                    There aren't any active alerts right now. When there's an active alert, it'll show up here.
                    Important alerts that you haven't read yet will also be highlighted on the home screen.
                </EmptyFeedback>
            )}
        </Screen>
    );
};