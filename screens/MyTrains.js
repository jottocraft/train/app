import React, { useContext, useMemo } from 'react';
import {
    Platform,
    Text,
    TouchableOpacity,
    View,
    ScrollView,
    Linking,
    Image
} from 'react-native';
import RoundIcon from '../components/RoundIcon';
import Screen from "../components/Screen";
import { timetableContext } from '../contexts/timetable';
import useTrainState from '../utils/trainState';
import usePref from '../contexts/prefs';
import SavedTrain from '../components/SavedTrain';
import EmptyFeedback from '../components/EmptyFeedback';

export default function MyTrainsScreen({ navigation }) {
    const { navigate } = navigation;

    const userTrains = usePref("userTrains");
    const timetable = useContext(timetableContext);
    const trips = timetable.trips;

    const savedUserTrains = useMemo(() => {
        if (!trips) return [];

        var active = [];
        Object.keys(userTrains.val).forEach(trainName => {
            var userTrain = userTrains.val[trainName];
            if (!userTrain.saved) return;

            var trainID = Object.keys(trips).find(t => trips[t].name == trainName);
            if (!trainID) return; //Skip trains that can't be found

            var train = trips[trainID];

            active.push(train);
        });

        return active;
    }, [timetable, userTrains]);

    const trainStates = useTrainState(null, ...savedUserTrains);

    return (
        <Screen fullWidth header={{ title: "Pinned Trains", icon: "push_pin" }}>
            <View style={{ marginTop: 20 }}>
                {trainStates?.length ? (
                    trainStates.map(nextStop => {
                        const train = trips[nextStop.id];

                        return (
                            <SavedTrain comfy key={nextStop.id} onPress={() => navigate('Train', { id: train.id })} nextStop={nextStop}>{train}</SavedTrain>
                        );
                    })
                ) : (
                    <EmptyFeedback title="No pinned trains">
                        You haven't pinned any trains yet. To get started, tap on a train, then press the "Pin" button.
                        Once you pin a train, it'll show up here, and, if it's active, on the home screen.
                    </EmptyFeedback>
                )}
            </View>
        </Screen>
    );
};