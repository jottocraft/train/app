import React, { useState, useEffect, useContext, useMemo, useCallback, useRef } from 'react';
import {
    Text,
    View,
    StatusBar
} from 'react-native';
import { useIsFocused } from '@react-navigation/native';

import RoundIcon from '../components/RoundIcon';

import { alertContext, streamContext, timetableContext } from "../contexts/timetable";
import themeContext from "../contexts/theme";

import { getRoundedAbsDelayMin, useTimeFormatter } from "../utils/time";
import useTrainState from '../utils/trainState';

import Alert from '../components/Alert';
import Screen from '../components/Screen';
import Button from '../components/Button';
import usePref from '../contexts/prefs';
import TouchEffect from '../components/TouchEffect';

export default function TrainScreen({ route, navigation }) {
    const { navigate } = navigation;

    const showEquipmentImages = usePref("showEquipmentImages");
    const userTrains = usePref("userTrains");
    const keepTimetableSFAtTop = usePref("keepTimetableSFAtTop");
    const debugUseArrBypass = usePref("debugUseArrBypass");
    const showStopBypasses = usePref("showStopBypasses");
    const delayPerStop = usePref("delayPerStop");
    const autoScrollToNextStop = usePref("autoScrollToNextStop");

    const formatTime = useTimeFormatter();

    const theme = useContext(themeContext);

    const alerts = useContext(alertContext);
    const stream = useContext(streamContext);

    const timetable = useContext(timetableContext);

    const trips = timetable.trips;
    const trip = trips[route?.params?.id];
    const stops = timetable.tracks[trip.track];
    const service = timetable.service;
    const future = route?.params?.future ?? false;

    var trainImage, trainEquipment;
    if (trip.equipment) {
        if (trip.equipment.includes("bomb")) {
            trainImage = require('../assets/images/optimized/trains/bomb.png');
            trainEquipment = trip.equipment.match(/\d+/)[0] + " car Bombardier";
        } else if (trip.equipment.includes("emu")) {
            trainImage = require('../assets/images/optimized/trains/emu.png');
            trainEquipment = trip.equipment.match(/\d+/)[0] + " car EMU";
        } else if (trip.equipment.includes("gal")) {
            trainImage = require('../assets/images/optimized/trains/gallery.png');
            trainEquipment = trip.equipment.match(/\d+/)[0] + " car gallery";
        }
    }

    const nextStop = useTrainState({ future }, trip)[0];
    const streamID = future ? "scheduled" : stream.id;

    const userTrain = useMemo(() => {
        if (!userTrains.val) return;
        return userTrains.val[trip.name];
    }, [trip.name, userTrains]);

    const toggleSavedTrain = useCallback(() => {
        if (!userTrain?.saved) userTrains.val[trip.name] = { saved: true };
        if (userTrain?.saved) delete userTrains.val[trip.name];
        userTrains.update({ ...userTrains.val });
    }, [userTrain, userTrains]);

    const trainAlerts = useMemo(() => {
        if (future || !alerts?.service) return [];
        return alerts.service.filter(alert => {
            if (alert.title && alert.title.includes(trip.name)) return true;
            if (alert.text && alert.text.includes(trip.name)) return true;

            return false;
        }).map(alert => {
            return (
                <View style={{ marginVertical: 5, marginHorizontal: theme.screenHorizPadding }}>
                    <Alert key={alert.id} {...alert} />
                </View>
            );
        });
    }, [alerts, trip, future]);

    const svRef = useRef();
    const [layout, setLayout] = useState(null);
    const [stuck, setStuck] = useState(false);

    const stickyHeight = theme.bigScreen ? 144 : 136;

    const stopBeforeNextStopLayoutCallback = useRef((() => {
        const layouts = {};
        return ({ container, stopBeforeNextStop }) => {
            if (container) layouts.container = container;
            if (stopBeforeNextStop) layouts.stopBeforeNextStop = stopBeforeNextStop;
            if (!layouts.container || !layouts.stopBeforeNextStop) return;
            stopBeforeNextStopLayoutCallback.current = null;
            if (!autoScrollToNextStop.val || !svRef.current) return;
            if (keepTimetableSFAtTop.val && (trip.dir == "NB")) {
                svRef.current.scrollTo({ y: layouts.stopBeforeNextStop.y + layouts.stopBeforeNextStop.height - layouts.container.height, animated: true });
            } else {
                svRef.current.scrollTo({ y: layouts.stopBeforeNextStop.y - stickyHeight, animated: true });
            }
        }
    })());

    const times = useMemo(() => {
        return keepTimetableSFAtTop.val && (trip.dir == "NB") ? trip.stops.slice().reverse() : trip.stops;
    }, [keepTimetableSFAtTop, trip.dir, trip.stops]);

    const scrollRef = useRef();
    const updateSticky = useCallback(() => {
        if (scrollRef.current === undefined) return;
        const stuck = scrollRef.current >= layout.y;
        setStuck(stuck);
    }, [layout]);
    useEffect(() => { updateSticky(); }, [updateSticky]);

    const isFocused = useIsFocused();
    const statusBarBg = stuck && !theme.bigScreen ? (theme.dark ? theme.ui[5] : theme.ui[0]) : theme.ui[0];

    return (
        <>
            {isFocused ? (
                <StatusBar backgroundColor={statusBarBg} style={theme.dark ? "light" : "dark"} translucent={false} />
            ) : null}
            <Screen onContainerLayout={e => {
                stopBeforeNextStopLayoutCallback.current?.({ container: e.nativeEvent.layout });
            }} svRef={svRef} fullWidth header={{
                title: `Caltrain ${trip.dir} ${trip.name}`,
                icon: "train",
                image: showEquipmentImages.val && trainImage,
                imageFit: "contain",
                tags: [
                    { text: service.regular.lineNames[trip.line].long, bg: theme.lineUI[trip.line], color: theme.lineUIText[trip.line] },
                    { text: trainEquipment, bg: theme.dark ? theme.ui[20] : theme.ui[0], color: theme.type[20] }
                ]
            }}
                stickyHeaderIndices={[trainAlerts.length]}
                scrollEventThrottle={16}
                onScroll={e => {
                    scrollRef.current = e.nativeEvent.contentOffset.y;
                    updateSticky();
                }}
                iosStatusBarBg={statusBarBg}>

                {trainAlerts}

                <View onLayout={e => setLayout(e.nativeEvent.layout)} style={{
                    height: stickyHeight,
                    paddingVertical: theme.bigScreen ? 4 : 0,
                    backgroundColor: theme.ui[0]
                }}>
                    <View style={{
                        ...(stuck ? {
                            backgroundColor: theme.dark ? theme.ui[5] : theme.ui[0],

                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 0.25,
                            shadowRadius: 3.84,

                            elevation: 5
                        } : {}),

                        ...(theme.bigScreen ? {
                            paddingHorizontal: theme.screenHorizPadding / 2,
                            marginHorizontal: theme.screenHorizPadding / 2,
                            borderRadius: 10,
                            overflow: "hidden"
                        } : {})
                    }}>
                        <View style={{
                            borderBottomWidth: 2,
                            
                            ...(stuck ? {
                                borderColor: theme.ui[30]
                            } : {
                                borderColor: theme.ui[0]
                            }),

                            ...(theme.bigScreen ? {
                                marginVertical: 4
                            } : {
                                paddingVertical: 4
                            }),

                            paddingHorizontal: theme.bigScreen ? 0 : theme.screenHorizPadding
                        }}>
                            <View style={{ height: 100, justifyContent: stuck ? "space-between" : "center", paddingVertical: 10 }}>
                                <View style={{ opacity: stuck ? 1 : 0, display: stuck ? "flex" : "none" }}>
                                    <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", marginBottom: 0 }}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                            <View style={{ position: 'relative', marginRight: 4 }}>
                                                <RoundIcon name="train" color={theme.type[0]} size={26} />
                                            </View>
                                            <View>
                                                <Text style={[{ color: theme.type[0] }, { fontFamily: theme.font.bold, fontSize: 22 }]}>{`Caltrain ${trip.dir} ${trip.name}`}</Text>
                                            </View>
                                        </View>
                                        <View style={{ flexDirection: "row" }}>
                                            <Text
                                                style={{
                                                    fontFamily: theme.font.medium,
                                                    color: theme.lineType[trip.line]
                                                }}
                                            >
                                                {service.regular.lineNames[trip.line].long}
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between" }}>
                                    {nextStop?.delay ? (
                                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                            <View style={{ position: 'relative', marginRight: 6 }}>
                                                <RoundIcon name={nextStop.icon} color={nextStop.color} size={24} />
                                            </View>
                                            <View>
                                                <Text style={[{ color: nextStop.color }, { fontFamily: theme.font.semibold, fontSize: 16 }]}>{nextStop.delay}</Text>
                                            </View>
                                        </View>
                                    ) : (
                                        <View />
                                    )}

                                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                                        {stuck && trainAlerts.length ? (
                                            <View style={{ marginRight: 5 }}>
                                                <Button small onPress={() => {
                                                    if (svRef.current) svRef.current.scrollTo({ y: 0, animated: true })
                                                }} icon="campaign">
                                                    {trainAlerts.length}
                                                </Button>
                                            </View>
                                        ) : null}
                                        <View>
                                            <Button small onPress={toggleSavedTrain} active={userTrain?.saved} icon="push_pin">
                                                {userTrain?.saved ? "Pinned" : "Pin"}
                                            </Button>
                                        </View>
                                    </View>
                                </View>
                            </View>

                            <View
                                style={{ paddingLeft: 22, flexDirection: 'row', alignItems: 'center', height: 26 }}>
                                <View style={{ flexGrow: 1 }}>
                                    <Text numberOfLines={1} style={{ fontSize: 14, fontFamily: theme.font.medium, textAlign: "left", color: theme.type[0] }}>Stop</Text>
                                </View>
                                <View style={{ marginHorizontal: 10 }}>
                                    {delayPerStop.val && (streamID !== "scheduled") ? (
                                        <Text style={{ width: 60, textAlign: "center", fontSize: 14, fontFamily: theme.font.medium, color: theme.type[0], textAlignVertical: "center" }}>Delay</Text>
                                    ) : (<Text style={{ width: 0 }}></Text>)}
                                </View>
                                <Text style={{ width: 85, textAlign: "right", fontSize: 14, fontFamily: theme.font.medium, color: theme.type[0] }}>ETD</Text>
                            </View>
                        </View>
                    </View>
                </View>

                <View style={{ height: 5 }} />

                {times.map((time, i) => {
                    //These 2 should be identical, except the NB one has the array reversed
                    const stopIndex = stops.findIndex(s => s.IDs.includes(time.id));
                    const stop = stops[stopIndex];
                    const isNextStop = nextStop.time && (time.id == nextStop.time.id);
                    const nextStopOffset = keepTimetableSFAtTop.val && (trip.dir == "NB") ? -1 : 1;
                    const isStopBeforeNextStop = nextStop.time && times[i + nextStopOffset] && (times[i + nextStopOffset].id == nextStop.time.id);
                    const delay = time[streamID].dep - time.scheduled.dep;
                    const delayMin = delayPerStop.val && getRoundedAbsDelayMin(delay);

                    const isPrevStop = nextStop.time && (time[streamID].dep < new Date());
                    const stopColor = isNextStop ? theme.type[0] : (isPrevStop ? theme.type[80] : theme.type[10]);
                    const delayColor = delay < 0 ? (theme.dark ? "#FDE047" : "#CA8A04") : (theme.dark ? "#EF4444" : "#B91C1C");

                    var bypasses = null;
                    if (time.bypassed && showStopBypasses.val) {
                        bypasses = time.bypassed.map((bypass) => {
                            var bypassStop = stops[bypass.stop];
                            var trueIndex = trip.stops.indexOf(time);
                            var timeBtw = new Date((debugUseArrBypass.val && time[streamID].arr) || time[streamID].dep).getTime() - new Date(trip.stops[trueIndex - 1][streamID].dep).getTime();
                            var bypassTime = new Date(trip.stops[trueIndex - 1][streamID].dep).getTime() + (timeBtw * bypass.percentage);

                            return (
                                <TouchEffect key={time.id} wide
                                    innerStyle={{ paddingVertical: 14, paddingLeft: 22, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                                    onPress={() => { navigate("HomeStack", { screen: "Station", params: { track: trip.track, i: bypass.stop } }) }}
                                >
                                    <Text numberOfLines={1} style={{ fontSize: 12, fontFamily: theme.font.italic, color: theme.type[100], flexShrink: 1 }}>{bypassStop.name}</Text>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', flexShrink: 0 }}>
                                        <Text style={{ width: 85, textAlign: "right", fontSize: 12, fontFamily: theme.font.italic, color: theme.type[100] }}>{formatTime(bypassTime)}</Text>
                                    </View>
                                </TouchEffect>
                            );
                        });
                    }

                    var thisStopJSX = (
                        <View key={time.id} onLayout={isStopBeforeNextStop ? e => {
                            stopBeforeNextStopLayoutCallback.current?.({ stopBeforeNextStop: e.nativeEvent.layout });
                        } : undefined}>
                            <TouchEffect wide
                                outerStyle={{
                                    backgroundColor: isNextStop ? theme.ui[10] : undefined
                                }}
                                innerStyle={{
                                    flexDirection: "row", alignItems: "center", paddingVertical: 16
                                }} onPress={() => { navigate("HomeStack", { screen: "Station", params: { track: trip.track, i: stopIndex } }) }}>
                                <View style={{ marginRight: 6 }}>{isNextStop ? (
                                    <View style={{ opacity: 0.75 }}>
                                        <RoundIcon name="flag" color={theme.type[0]} size={16} />
                                    </View>
                                ) : (
                                    <View style={{ opacity: 0.5 }}>
                                        <RoundIcon name={keepTimetableSFAtTop.val && (trip.dir == "NB") ? "keyboard_arrow_up" : "keyboard_arrow_down"} color={theme.type[0]} size={16} />
                                    </View>
                                )}</View>
                                <View style={{ flexGrow: 1 }}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                        <Text numberOfLines={1} style={{ fontSize: isNextStop ? 16 : 12, fontFamily: isNextStop ? theme.font.bold : theme.font.medium, color: stopColor, flexShrink: 1 }}>{stop.name}</Text>
                                        <View style={{ flexDirection: 'row', alignItems: 'center', flexShrink: 0 }}>
                                            {delayMin ? (<Text style={{ opacity: isPrevStop ? 0.6 : 1, width: 60, textAlign: "left", fontSize: 10, fontFamily: isNextStop ? theme.font.bold : theme.font.medium, color: delayColor, textAlignVertical: 'center' }}>{
                                                (delay < 0 ? "-" : "+") + String(delayMin) + " min"
                                            }</Text>) : (<Text style={{ width: 0 }}></Text>)}
                                            <Text style={{ width: 85, textAlign: "right", fontSize: isNextStop ? 16 : 12, fontFamily: isNextStop ? theme.font.bold : theme.font.medium, color: stopColor }}>{formatTime(time[streamID].dep)}</Text>
                                        </View>
                                    </View>
                                </View>
                            </TouchEffect>
                        </View>
                    );

                    if (keepTimetableSFAtTop.val && (trip.dir == "NB")) return <>{thisStopJSX}{bypasses && bypasses.reverse()}</>;
                    return <>{bypasses}{thisStopJSX}</>;
                })}
            </Screen>
        </>
    );
};