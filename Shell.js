import React, { useContext, useEffect } from "react";
import { Platform, View, Text } from "react-native";
import { NavigationContainer, useNavigation } from "@react-navigation/native";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { StatusBar } from "expo-status-bar";
import { useFonts } from "expo-font";
import Constants from "expo-constants";
import * as NavigationBar from "expo-navigation-bar";
import * as SystemUI from 'expo-system-ui';
import * as SplashScreen from 'expo-splash-screen';
import { useUpdates } from "expo-updates";

import { timetableContext } from "./contexts/timetable.js";
import themeContext from "./contexts/theme.js";

import SplitStack, { createSplitStackNavigator } from "./components/SplitStack.js";
import { createTabNavigatorPlusPlus } from "./components/TabNavigatorPlusPlus.js";

import HomeScreen from "./screens/Home";
import SettingsScreen from "./screens/Settings";
import TimetableScreen from "./screens/Timetable";
import EventScreen from "./screens/Event";
import NotificationScreen from "./screens/Notification";
import AlertScreen from "./screens/Alerts";
import StationScreen from "./screens/Station";
import TrainScreen from "./screens/Train";
import DebugScreen from './screens/Debug';
import MyTrainsScreen from './screens/MyTrains.js';
import PickerScreen from "./screens/Picker.js";
import OTAUpdateScreen from "./screens/OTAUpdate.js";
import EmptyAsideScreen from "./screens/EmptyAside.js";

const HomeStack = createSplitStackNavigator();
const HomeAsideStack = createNativeStackNavigator();
const homeRoutes = {
  Alerts: AlertScreen,
  MyTrains: MyTrainsScreen,
  Train: TrainScreen,
  Station: StationScreen,
  Event: EventScreen,
  Notification: NotificationScreen
};
function HomeNavigator() {
  const theme = useContext(themeContext);

  return (
    <SplitStack aside={(
      <HomeAsideStack.Navigator screenOptions={{ headerShown: false }} initialRouteName="Alerts">
        {Object.keys(homeRoutes).map(route => <HomeAsideStack.Screen key={route} name={route} component={homeRoutes[route]} />)}
      </HomeAsideStack.Navigator>
    )}>
      <HomeStack.Navigator screenOptions={{ headerShown: false }} initialRouteName="Home">
        <HomeStack.Screen name="Home" component={HomeScreen} />
        <HomeStack.Screen name="Debug" component={DebugScreen} />
        {theme.bigScreen ? null : Object.keys(homeRoutes).map(route => <HomeStack.Screen key={route} name={route} component={homeRoutes[route]} />)}
      </HomeStack.Navigator>
    </SplitStack>
  )
}

const TimetableStack = createSplitStackNavigator();
const TimetableAsideStack = createNativeStackNavigator();
const timetableRoutes = {
  Train: TrainScreen,
  Picker: PickerScreen
};
function TimetableNavigator() {
  const theme = useContext(themeContext);

  return (
    <SplitStack aside={(
      <TimetableAsideStack.Navigator screenOptions={{ headerShown: false }} initialRouteName="EmptyAside">
        <TimetableAsideStack.Screen name="EmptyAside" component={EmptyAsideScreen} />
        {Object.keys(timetableRoutes).map(route => <TimetableAsideStack.Screen key={route} name={route} component={timetableRoutes[route]} />)}
      </TimetableAsideStack.Navigator>
    )}>
      <TimetableStack.Navigator screenOptions={{ headerShown: false }} initialRouteName="Home">
        <TimetableStack.Screen name="Timetable" component={TimetableScreen} />
        {theme.bigScreen ? null : Object.keys(timetableRoutes).map(route => <TimetableStack.Screen key={route} name={route} component={timetableRoutes[route]} />)}
      </TimetableStack.Navigator>
    </SplitStack>
  )
}

const SettingsStack = createNativeStackNavigator();
function SettingsNavigator() {
  return (
    <SettingsStack.Navigator screenOptions={{ headerShown: false }} initialRouteName="Settings">
      <SettingsStack.Screen name="Settings" component={SettingsScreen} />
      <SettingsStack.Screen name="OTAUpdate" component={OTAUpdateScreen} />
    </SettingsStack.Navigator>
  )
}

const RootTabs = createTabNavigatorPlusPlus();
function RootNavigator() {
  const updates = useUpdates();
  const navigation = useNavigation();

  useEffect(() => {
    if (updates.isUpdatePending && updates.availableUpdate?.manifest?.metadata?.critical) {
      navigation.reset({
        index: 0,
        routes: [
          {
            name: "SettingsStack",
            state: {
              index: 1,
              routes: [
                { name: "Settings" },
                { name: "OTAUpdate" }
              ]
            }
          }
        ]
      });
    }
  }, [updates.availableUpdate, updates.isUpdatePending, navigation]);

  return (
    <RootTabs.Navigator initialRouteName="HomeStack">
      <RootTabs.Screen options={{ label: "Trains", icon: "train" }} name="HomeStack" component={HomeNavigator} />
      <RootTabs.Screen options={{ label: "Trip Planner", icon: "rebase_edit" }} name="TimetableStack" component={TimetableNavigator} />
      <RootTabs.Screen options={{ label: "Settings", icon: "settings" }} name="SettingsStack" component={SettingsNavigator} />
    </RootTabs.Navigator>
  );
}

export default function Shell() {
  const theme = useContext(themeContext);
  const timetable = useContext(timetableContext);

  const [fontsLoaded] = Constants.executionEnvironment === "bare" ? [true] : useFonts({
    "PublicSans-Italic": require("./assets/fonts/PublicSans-Italic.ttf"),
    "PublicSans-Light": require("./assets/fonts/PublicSans-Light.ttf"),
    "PublicSans-Regular": require("./assets/fonts/PublicSans-Regular.ttf"),
    "PublicSans-Medium": require("./assets/fonts/PublicSans-Medium.ttf"),
    "PublicSans-SemiBold": require("./assets/fonts/PublicSans-SemiBold.ttf"),
    "PublicSans-Bold": require("./assets/fonts/PublicSans-Bold.ttf"),
    "RobotoMono-Bold": require("./assets/fonts/RobotoMono-Bold.ttf"),
    "MaterialSymbolsRounded24pt-Regular": require("./assets/fonts/MaterialSymbolsRounded24pt-Regular.ttf")
  });

  const isReady = timetable && theme && fontsLoaded;

  if (isReady && !global.wasReady) {
    global.wasReady = true;
    console.log("State ready after " + (new Date().getTime() - global.appMountedAt) + "ms");
  }

  useEffect(() => {
    if (!isReady) return;
    SystemUI.setBackgroundColorAsync(theme.ui[0]);

    if (Platform.OS === "android") {
      NavigationBar.setBackgroundColorAsync(theme.bigScreen ? theme.ui[0] : (theme.dark ? theme.ui[10] : theme.ui[0]));
      NavigationBar.setButtonStyleAsync(theme.dark ? "light" : "dark");
    }

    if (Platform.OS === "web") {
      document.documentElement.style["color-scheme"] = theme.dark ? "dark" : "light";
    }
  }, [theme, isReady]);

  return isReady ? (
    <>
      <NavigationContainer
        theme={{
          dark: theme.dark,
          colors: {
            background: theme.ui[0]
          }
        }}
        onReady={() => {
          console.log("Initial screen ready after " + (new Date().getTime() - global.appMountedAt) + "ms");
          SplashScreen.hideAsync();
        }}
        documentTitle={{ enabled: false }}>
        <RootNavigator />
      </NavigationContainer>
      <StatusBar backgroundColor={theme.ui[0]} style={theme.dark ? "light" : "dark"} translucent={false} />
    </>
  ) : (__DEV__ ? (
    <View>
      {!timetable ? <Text style={{ backgroundColor: "red", padding: 50 }}>Waiting for timetable...</Text> : null}
      {!theme ? <Text style={{ backgroundColor: "orange", padding: 50 }}>Waiting for theme...</Text> : null}
      {!fontsLoaded ? <Text style={{ backgroundColor: "yellow", padding: 50 }}>Waiting for fonts...</Text> : null}
    </View>
  ) : null);
};;
