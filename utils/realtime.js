//This file fetches and distributes realtime data to components that need it
//It also ensures the timetable is up-to-date

import { useEffect, useMemo, useRef, useState, useCallback } from 'react';
import {
    Platform
} from 'react-native';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useNetInfo } from "@react-native-community/netinfo";
import "react-native-get-random-values";
import { v4 as uuidv4 } from 'uuid';
import Constants from "expo-constants";

import { getCurrentServiceBaseMs, initTimetable } from "../utils/initTimetable";
import usePref from '../contexts/prefs';
import { getVersion } from './version';


//If the search yielded multiple potential candidates,
//we may be able to choose one based on if it's in service today or not
function pickFromMultipleMatches(trains, matches) {
    let activeTrainMatch = null;
    for (let i = 0; i < matches.length; i++) {
        const id = matches[i];
        if (trains[id].inService) {
            if (!activeTrainMatch) {
                activeTrainMatch = id;
            } else {
                return null;
            }
        }
    }

    return activeTrainMatch;
}

//Finds a train by its ID or name
//Tries to resolve as many potential mismatches as possible (very inefficient)
//In theory this is never needed. If the IDs match exactly (as they should), no expensive searches are needed
function searchForTrainID(trains, search) {
    //Try using search as the exact ID (theoretically should always be the case)
    if (trains[search]) return search;

    const trainIDs = Object.keys(trains);

    //Try using search as the short name
    const trainShortNameMatches = trainIDs.filter(id => trains[id].name === search);
    const trainShortNamePick = pickFromMultipleMatches(trains, trainShortNameMatches);
    if (trainShortNamePick) return trainShortNamePick;

    //Try using search as a substring of the local ID
    const trainIDSubstrMatches = trainIDs.filter(id => id.includes(search));
    const trainIDSubstrPick = pickFromMultipleMatches(trains, trainIDSubstrMatches);
    if (trainIDSubstrPick) {
        console.warn("[searchForTrainID] Dangerously matched id substring to resolve mismatch", search, trainIDSubstrPick);
        return trainIDSubstrPick;
    }

    //Try using search as a substring of the short name
    const trainNameSubstrMatches = trainIDs.filter(id => trains[id].name.includes(search));
    const trainNameSubstrPick = pickFromMultipleMatches(trains, trainNameSubstrMatches);
    if (trainNameSubstrPick) {
        console.warn("[searchForTrainID] Dangerously matched short_name substring to resolve mismatch", search, trainNameSubstrPick);
        return trainNameSubstrPick;
    }

    //Try extracting a 3 digit train ID from search and recursively try again
    const threeDigitMatch = search.match(/(\d\d\d)/);
    const threeDigitVal = threeDigitMatch && threeDigitMatch[1];
    if (threeDigitVal && (threeDigitVal !== search)) return searchForTrainID(trains, search);

    //Nothing works :(
    console.warn("[searchForTrainID] Failed to find a match for realtime ID", search);
    return null;
}

export default function useRealtimeData(timetable, setTimetable) {
    const netInfo = useNetInfo();

    const dev = usePref("dev");
    const selectedStream = usePref("selectedStream");
    const debugRealtimeError = usePref("debugRealtimeError");
    const debugRealtimeDevelopment = usePref("debugRealtimeDevelopment");
    const iid = usePref("iid");

    const [serverResultingState, setServerResultingState] = useState(null);
    const timetableRef = useRef(timetable);
    useEffect(() => { timetableRef.current = timetable; }, [timetable]);

    const canUseRealtime = useMemo(() => {
        if (netInfo.isConnected === null) {
            setServerResultingState(null);

            return false;
        } else if (netInfo.isConnected === false) {
            setServerResultingState({
                alerts: {
                    realtime: {
                        title: "Device Offline",
                        text: "Your device is offline. Please connect to the internet to get realtime data."
                    }
                }
            });

            return false;
        }

        if (timetable && (selectedStream.val !== "scheduled")) {
            return true;
        }

        return false;
    }, [Boolean(timetable), selectedStream, netInfo.isConnected]);

    const updateRealtime = useCallback(async () => {
        if (!timetableRef.current) return;

        if (timetableRef.current.serviceBaseMs !== getCurrentServiceBaseMs()) {
            console.log("Re-initializing timetable for new service day");

            const timetableReinit = { ...initTimetable(timetableRef.current) };
            timetableRef.current = timetableReinit;
            setTimetable(timetableReinit);
        }

        if (!canUseRealtime) return;

        console.log("CALL");

        let newIID;
        if (!iid.val) {
            newIID = uuidv4();
            iid.update(newIID);
        }

        return new Promise(resolveFin => {
            var responseHeaders;
            const requestHeaders = {
                "AliceSprings-useragent": timetableRef.current.liveasset.ua,
                "AliceSprings-clock": new Date().getTime(),
                "AliceSprings-iid": newIID ?? iid.val,
                "AliceSprings-liveasset": timetableRef.current.liveasset.ver,
                "AliceSprings-platform": Platform.OS
            }
            const OTA = getVersion().OTA;
            if (typeof OTA === "number") requestHeaders["AliceSprings-ota"] = OTA;
            if (typeof Constants.expoConfig?.runtimeVersion === "string") requestHeaders["AliceSprings-runtime"] = Constants.expoConfig.runtimeVersion;
            fetch(debugRealtimeDevelopment.val ? 'https://train-dev.hq.jottocraft.com/api/transit/realtime/' : 'https://train.jottocraft.com/api/transit/realtime/', {
                headers: requestHeaders
            })
                .catch((error) => {
                    console.error("Realtime data fetch error", error);

                    setServerResultingState({
                        alerts: {
                            realtime: {
                                title: "Connection Error",
                                text: "Failed to connect to the realtime data server"
                            }
                        }
                    });

                    return Promise.reject(8804);
                })
                .then(response => {
                    responseHeaders = response.headers;
                    return response.json();
                })
                .then(data => {
                    if (responseHeaders.get("AliceSprings-Response") === "error") {
                        setServerResultingState({
                            alerts: {
                                realtime: data
                            }
                        });

                        return Promise.reject(8804);
                    } else if (responseHeaders.get("AliceSprings-Response") === "liveassetPatch") {
                        return updateTimetable(data, timetableRef.current);
                    } else if (responseHeaders.get("AliceSprings-Response") === "patch") {
                        return Promise.resolve({ data, latestTimetable: timetableRef.current });
                    }
                })
                .then(({ data, latestTimetable, timetableUpdate }) => {
                    //Realtime data warning for incomplete application of realtime data
                    let warning;

                    //Count the # of train ID and stop ID mismatches
                    const warningCounts = {
                        trainIDMismatch: 0,
                        stopIDMismatch: 0
                    };

                    //Reset online status for each train
                    Object.values(latestTimetable.trips).forEach(t => t.online = false);

                    //Add realtime data to each train
                    Object.keys(data.trips).forEach(realtimeID => {
                        //Search for the local train ID based on realtime data
                        const localID = searchForTrainID(latestTimetable.trips, realtimeID);

                        if (localID === null) {
                            warningCounts.trainIDMismatch++;
                            return;
                        }

                        //Get the realtime and local train objects
                        const realtimeTrain = data.trips[realtimeID];
                        const localTrain = latestTimetable.trips[localID]; //if a localID is returned, it is guaranteed to exist on localTimetable

                        //Mark the local train as online
                        localTrain.online = true;

                        //Apply delay info for each stop
                        for (const stopID in realtimeTrain.stops) {
                            const realtimeStop = realtimeTrain.stops[stopID];

                            const localStop = localTrain.stops.find(s => s.id === stopID);

                            if (!localStop) {
                                warningCounts.stopIDMismatch++;
                                continue;
                            }

                            localStop.real = {
                                dep: realtimeStop.dep && new Date(realtimeStop.dep),
                                arr: realtimeStop.arr && new Date(realtimeStop.arr)
                            };
                        }

                        // Assume the delay for the last stop is the same as the one before it (since it doesn't depart, no data is provided from 511)
                        const secondLastStop = localTrain.stops[localTrain.stops.length - 2];
                        const lastStop = localTrain.stops[localTrain.stops.length - 1];
                        const secondLastDelay = { dep: new Date(secondLastStop.real.dep).getTime() - new Date(secondLastStop.scheduled.dep).getTime(), arr: new Date(secondLastStop.real.arr).getTime() - new Date(secondLastStop.scheduled.arr).getTime() };
                        lastStop.real.dep = new Date(new Date(lastStop.scheduled.dep).getTime() + secondLastDelay.dep);
                        lastStop.real.arr = new Date(new Date(lastStop.scheduled.arr).getTime() + secondLastDelay.arr);

                        realtimeTrain.stops = { ...realtimeTrain.stops };
                    });

                    if (warningCounts.trainIDMismatch || warningCounts.stopIDMismatch) {
                        warning = `Some data could not be applied  [${warningCounts.trainIDMismatch},${warningCounts.stopIDMismatch}]`;
                    }

                    //Pass on data and timetableUpdate state for alert processing
                    return { data, latestTimetable, timetableUpdate, warning };
                })
                .then(({ data, latestTimetable, timetableUpdate, warning }) => {
                    setServerResultingState({
                        ao: data.ao,
                        warning,
                        alerts: {
                            service: data.alerts
                        },
                        timetable: latestTimetable,
                        timetableUpdate
                    });
                })
                .catch((error) => {
                    //Error already handled
                    if (error === 8804) return console.log("Promise chain cancelled realtime");

                    console.error(error);

                    setServerResultingState({
                        alerts: {
                            realtime: {
                                title: "Client Error",
                                text: "Failed to process realtime data"
                            }
                        }
                    });
                }).finally(() => {
                    resolveFin();
                });
        });
    }, [canUseRealtime, timetableRef, debugRealtimeDevelopment, iid, dev]);

    useEffect(() => {
        console.log("Setting realtime timer");

        updateRealtime();
        const fetchInterval = setInterval(updateRealtime, 30000);
        return () => clearInterval(fetchInterval);
    }, [updateRealtime]);

    const result = useMemo(() => {
        console.log("New realtime result");
        if (debugRealtimeError.val) {
            return {
                alerts: {
                    realtime: {
                        title: "Debug Error",
                        text: "Realtime data has thrown an error for debugging purposes."
                    }
                }
            };
        } else {
            return serverResultingState;
        }
    }, [debugRealtimeError, serverResultingState]);

    return { result, canUseRealtime, updateRealtime };
}

function updateTimetable(timetablePatch, oldTimetable) {
    console.log("[TIMETABLE UPDATE]");
    const latestTimetable = timetablePatch.liveasset;
    const realtimePatch = timetablePatch.patch;

    return new Promise((resolve, reject) => {
        if (latestTimetable.liveasset) {
            //Save timetable
            AsyncStorage.setItem("timetable", JSON.stringify(latestTimetable)).then(() => {
                //Set update
                var newTimetable = initTimetable(latestTimetable);
                resolve({ data: realtimePatch, latestTimetable: newTimetable, timetableUpdate: { prevName: oldTimetable?.liveasset?.name } });
                //reject({ //break promise chain to prevent realtime processing
                //    cancel: [
                //        true,
                //        resolve({ //resolve main promise with new data
                //            timetable: latestTimetable,
                //            timetableUpdate: true
                //        })
                //    ]
                //});
            });
        } else {
            console.error("Got updated timetable but it doesn't have a .liveasset for some reason?");
        }
    });
}