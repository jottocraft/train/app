const moment = require('moment-timezone');

const SECOND = 1000;
const MINUTE = 60 * SECOND;
const HOUR = 60 * MINUTE;

function getDate(base, time) {
    let ms = base;
    
    const [hours, minutes, seconds] = time.split(":");
    ms += Number(hours) * HOUR;
    ms += Number(minutes) * MINUTE;
    ms += Number(seconds) * SECOND;

    return new Date(ms);
}

function parseGTFSDate(gtfsDateString) {
    const year = Number(String(gtfsDateString).substring(4, 0));
    const monthIndex = Number(String(gtfsDateString).substring(4).substring(2, 0)) - 1;
    const date = Number(String(gtfsDateString).substring(6));

    return new Date(year, monthIndex, date);
}

/**
 * Compares only the date components of 2 dates
 * @param {Date} d1 
 * @param {Date} d2 
 * @returns A positive integer if d1 is bigger; a negative integer if d2 is bigger; zero if they are the same day
 */
function compareDates(d1, d2) {
    //Compare years
    const yearDiff = d1.getFullYear() - d2.getFullYear();
    if (yearDiff !== 0) return yearDiff;

    //Compare months
    const monthDiff = d1.getMonth() - d2.getMonth();
    if (monthDiff !== 0) return monthDiff;

    //Compare date
    const dateDiff = d1.getDate() - d2.getDate();
    if (dateDiff !== 0) return dateDiff;

    return 0;
}

export function getService(service, date = new Date()) {
    var removedService = [];
    var todaysService = [];
    var specialService = false;

    //loop over special service
    service.dates.forEach(sp => {
        var specialDate = parseGTFSDate(sp.date);

        if (!compareDates(date, specialDate)) {
            //Running special service
            specialService = true;
            if (sp.exception_type == 1) {
                todaysService.push(sp.service_id);
            } else {
                if (sp.exception_type == 2) {
                    removedService.push(sp.service_id);
                }
            }
        }
    });

    //loop over regular service for today
    service.regular.days[moment.tz(date, "America/Los_Angeles").day()].forEach(serviceID => {
        //Removed from service (special service modification)
        if (removedService.includes(serviceID)) return;

        //Not in service (outside service date scope)
        if (compareDates(parseGTFSDate(service.regular.ranges[serviceID].start), date) > 1) return;
        if (compareDates(parseGTFSDate(service.regular.ranges[serviceID].end), date) < 1) return;

        //Not removed; add
        todaysService.push(serviceID);
    });

    return todaysService;
}

export function getCurrentServiceBaseMs() {
    const isBefore3AM = moment.tz("America/Los_Angeles").hour() < 3;
    const serviceBaseMs = new Date(moment.tz("America/Los_Angeles").startOf("day").subtract(isBefore3AM ? 1 : 0, "day").format()).getTime();

    return serviceBaseMs;
}

export function initTimetable(newTimetable) {
    const service = getService(newTimetable.service);

    const serviceBaseMs = getCurrentServiceBaseMs();
    newTimetable.serviceBaseMs = serviceBaseMs;
    newTimetable.expired = compareDates(new Date(serviceBaseMs), parseGTFSDate(newTimetable.liveasset.expiry)) > 0;

    for (const trackID in newTimetable.tracks) {
        for (const stop of newTimetable.tracks[trackID]) stop.inService = false;
    }

    for (const id in newTimetable.trips) {
        const trip = newTimetable.trips[id];
        trip.inService = service.includes(trip.service);
        trip.online = false;

        for (const stop of trip.stops) {
            const dep = getDate(serviceBaseMs, stop.timetable.dep), arr = getDate(serviceBaseMs, stop.timetable.arr);

            stop.scheduled = { dep, arr };
            stop.real = { dep, arr };

            if (trip.inService) newTimetable.tracks[trip.track].find(s => s.IDs.includes(stop.id)).inService = true;
        }
    }

    return newTimetable;
}