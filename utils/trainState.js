import { useEffect, useState, useMemo, useContext } from "react";
import { streamContext, timetableContext } from "../contexts/timetable";
import { formatDelay } from "./time";
import themeContext from "../contexts/theme";

export default function useTrainState(options, ...trains) {
    const stream = useContext(streamContext);
    const theme = useContext(themeContext);
    const future = options?.future ?? false;

    const timetable = useContext(timetableContext);

    const streamID = useMemo(() => {
        if (future) return "scheduled";
        return stream.id;
    }, [future, stream]);

    const [date, setDate] = useState(new Date());
    const nextStops = useMemo(() => {
        var data = [];

        trains.forEach(train => {
            if (future) {
                return data.push({
                    id: train.id,
                    time: null,
                    delay: new Date(future).toDateString(),
                    color: theme.streamType.trainOffline,
                    icon: "date_range"
                });
            }

            if (!train.inService) {
                return data.push({ id: train.id, time: null });
            }

            let time = null;
            for (const stop of train.stops) {
                if (date >= stop[streamID].dep) continue; //Previous stop
                if (time === null) {
                    //No existing next stop; use this one
                    time = stop;
                    break;
                }
            }

            if ((train.stops.indexOf(time) === 0) && !train.online) {
                //Consider train inactive since it has no data and hasn't gone anywhere yet
                return data.push({ id: train.id, time: null });
            } else if (time === null) {
                return data.push({ id: train.id, time: null });
            } else {
                return data.push({
                    id: train.id,
                    time,
                    stop: timetable.tracks[train.track].find(s => s.IDs.includes(time.id)),
                    delay: stream.hasData && (streamID !== "scheduled") ? (
                        !train.online ? "No data" : formatDelay(time[streamID].dep - time.scheduled.dep)
                    ) : stream.text,
                    color: stream.hasData && (streamID !== "scheduled") && !train.online ? theme.streamType.trainOffline : theme.streamType[stream.color],
                    icon: stream.icon
                });
            }
        });

        return data;
    }, [trains, timetable, stream, future, streamID, date, theme]);

    useEffect(() => {
        var nearestTime = Math.min(...(nextStops.filter(stop => stop.time).map(stop => stop.time[streamID].dep)));
        if (!nearestTime || (nearestTime == Infinity)) return;

        var updateNextStops = setTimeout(() => {
            console.log("Updating next stops...");
            setDate(new Date());
        }, nearestTime - new Date());

        return () => clearTimeout(updateNextStops);
    }, [nextStops, streamID]);

    return nextStops;
}