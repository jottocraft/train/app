import { Platform } from "react-native";
import * as base64 from "base-64";
import Constants from "expo-constants";
import * as Application from "expo-application";

import OTA_CHANNELS from "../channels";

export function getVersion({ MAJOR: MAJOR_OVERRIDE, MINOR: MINOR_OVERRIDE, BUILD: BUILD_OVERRIDE, OTA: OTA_OVERRIDE, channel: channel_override } = {}) {
    if (Platform.OS === "web") {
        return {
            name: Constants.expoConfig?.version ?? null,
            number: ""
        };
    }

    let nativeVersion = null;
    if (Constants.expoConfig?.updates?.requestHeaders?.["jottocraft-Native-App-Version"]) {
        try {
            nativeVersion = JSON.parse(base64.decode(Constants.expoConfig.updates.requestHeaders["jottocraft-Native-App-Version"]));
        } catch (e) {
            console.error("Failed to parse native version updates header", e);
        }
    }

    if (nativeVersion) {
        const MAJOR = MAJOR_OVERRIDE ?? nativeVersion.MAJOR;
        const MINOR = MINOR_OVERRIDE ?? nativeVersion.MINOR;
        const BUILD = BUILD_OVERRIDE ?? nativeVersion.BUILD;
        const OTA = OTA_OVERRIDE ?? Constants.manifest2?.metadata?.otaVersion ?? nativeVersion.OTA;

        const channel = channel_override ?? Constants.expoConfig?.updates?.requestHeaders?.["Expo-Channel-Name"];

        return {
            name: `${MAJOR}.${MINOR}.${BUILD}-${OTA}${channel ? OTA_CHANNELS[channel].suffix : ""}`,
            number: MAJOR * Math.pow(10, 8) + MINOR * Math.pow(10, 6) + BUILD * Math.pow(10, 4) + OTA * 10 + (channel ? OTA_CHANNELS[channel].value : 0),
            MAJOR,
            MINOR,
            BUILD,
            OTA,
            channel
        };
    }

    return {
        name: Constants.expoConfig?.version ?? null,
        number: Constants.executionEnvironment + " " + Application.nativeBuildVersion
    };
}