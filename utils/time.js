import moment from "moment-timezone";
import usePref from "../contexts/prefs";
import { useCalendars } from "expo-localization";
import { useCallback } from "react";

export const getRoundedAbsDelayMin = ms => {
    const totalSeconds = Math.round(Math.abs(ms) / 1000);

    const min = Math.floor(totalSeconds / 60);
    const sec = totalSeconds - min * 60;

    // 30 sec delays are commonly reported for on-time trains; round them down
    const rounded = min + (sec > 30 ? 1 : 0);

    return rounded;
};

export const formatDelay = ms => {
    if (!ms) return "On time";

    const rounded = getRoundedAbsDelayMin(ms);

    if (!rounded) return "On time";
    if (ms > 0) return `${rounded} ${rounded === 1 ? "minute" : "minutes"} late`;
    if (ms < 0) return `${rounded} ${rounded === 1 ? "minute" : "minutes"} early`;
}

export const useTimeFormatter = () => {
    const showSeconds = usePref("showSeconds");
    const calendars = useCalendars();
    const prefers24Hour = calendars?.[0]?.uses24hourClock;

    return useCallback((date) => {
        return moment(date).format(prefers24Hour ? (showSeconds.val ? 'HH:mm:ss' : 'HH:mm') : (showSeconds.val ? 'h:mm:ss' : 'h:mm A'));
    }, [prefers24Hour, showSeconds]);
}