import { useEffect } from "react";
import { useAnimatedStyle, useSharedValue, withDelay, withRepeat, withSequence, withTiming } from "react-native-reanimated";

export function useBlinkAnimation() {
    const blink = useSharedValue(0.8);
    useEffect(() => {
        blink.value = withRepeat(withSequence(withTiming(0.25, { duration: 400 }), withDelay(50, withTiming(0.8, { duration: 400 }))), -1, false);
        return () => blink.value = 0.8;
    }, []);

    const blinkAnimation = useAnimatedStyle(() => {
        return {
            opacity: blink.value
        }
    });

    return blinkAnimation;
}