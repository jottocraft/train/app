const path = require("path");
const { withDangerousMod } = require("expo/config-plugins");
const { readXMLAsync, writeXMLAsync } = require("@expo/config-plugins/build/utils/XML");

module.exports = function withXcodeDebugConfig(config) {
    return withDangerousMod(config, [
        "ios",
        async (config) => {
            if (process.env.TRAIN_BUILD_MODE !== "debug") return config;
            
            const schemePath = path.join(config.modRequest.projectRoot, "ios/Train.xcodeproj/xcshareddata/xcschemes/Train.xcscheme");

            const scheme = await readXMLAsync({
                path: schemePath
            });

            scheme.Scheme.ArchiveAction[0].$.buildConfiguration = "Debug";

            await writeXMLAsync({
                path: schemePath,
                xml: scheme
            });

            return config;
        }
    ]);
}