const { AndroidConfig, withAndroidStyles } = require("expo/config-plugins");

module.exports = function withAndroidMaterial3(config) {
    return withAndroidStyles(config, styles => {
        const appTheme = AndroidConfig.Styles.getStyleParent(styles.modResults, { name: "AppTheme" });
        appTheme.$.parent = "Theme.Material3.DayNight.NoActionBar";
        return styles;
    });
}