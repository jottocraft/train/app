const { withXcodeProject } = require("expo/config-plugins");
const { findFirstNativeTarget } = require("@expo/config-plugins/build/ios/Target");
const { getBuildConfigurationsForListId } = require("@expo/config-plugins/build/ios/utils/Xcodeproj");

module.exports = function withXcodeProductName(config) {
    return withXcodeProject(config, mod => {
        const project = mod.modResults;
        const [, nativeTarget] = findFirstNativeTarget(project);

        getBuildConfigurationsForListId(project, nativeTarget.buildConfigurationList).forEach(
            ([, item]) => {
                item.buildSettings.PRODUCT_NAME = config.nativeProjectName;
            }
        );

        return mod;
    });
}