// Makes a distribution for deployment on aero.jottocraft.com

const fs = require("fs");
const path = require("path");
const uuid = require("uuid");
const crypto = require("crypto");
const { rimrafSync } = require("rimraf");
const ExpoConfig = require("@expo/config");

const OUTPUT_DIR = path.join(__dirname, "dist", "aero", "train", "expo");

rimrafSync(path.join(__dirname, "dist", "aero"));
fs.mkdirSync(path.join(__dirname, "dist", "aero"));
fs.mkdirSync(path.join(__dirname, "dist", "aero", "train"));
fs.mkdirSync(path.join(__dirname, "dist", "aero", "train", "expo"));

if (process.env.TRAIN_NATIVE_BUILD) throw new Error("Cannot create an OTA release with native build environment variables set.");

const { exp } = ExpoConfig.getConfig(__dirname, {
    skipSDKVersionRequirement: true,
    isPublicConfig: true
});

const exportMetadata = JSON.parse(String(fs.readFileSync(path.join(__dirname, "dist", "metadata.json"))));

console.log(`Creating Aero Expo OTA distribution for Caltrain Live ${exp.version}; CURRENT_VERSION.OTA is ${exp.extra.otaVersion}`);

if (typeof exp.extra.otaVersion !== "number") throw new Error("Expected a valid OTA version set at exp.extra.otaVersion.");

import("mime").then(mime => {
    function copyAssetAndGetMetadata(location, ext, omitFileExtension = false) {
        const file = fs.readFileSync(location);

        const asset = {
            hash: crypto.createHash("sha256").update(file).digest("base64url"),
            key: crypto.createHash("md5").update(file).digest("hex"),
            contentType: mime.default.getType(ext)
            // URL added by Aero
        };

        if (!omitFileExtension) asset.fileExtension = "." + ext;

        // putting everything into the same folder = automatic asset de-duplication between releases :)
        // the extension should be added here because R2 will get it wrong
        fs.copyFileSync(location, path.join(OUTPUT_DIR, asset.key + "." + ext));

        return asset;
    }

    for (const platform of ["android", "ios"]) {
        const expoUpdateManifest = {};

        expoUpdateManifest.id = uuid.v4(); // Hopefully this never collides
        expoUpdateManifest.createdAt = new Date().toISOString();
        expoUpdateManifest.runtimeVersion = exp.runtimeVersion;

        expoUpdateManifest.launchAsset = copyAssetAndGetMetadata(path.join(__dirname, "dist", exportMetadata.fileMetadata[platform].bundle), "js", true);

        expoUpdateManifest.assets = [];
        for (const asset of exportMetadata.fileMetadata[platform].assets) {
            expoUpdateManifest.assets.push(copyAssetAndGetMetadata(path.join(__dirname, "dist", asset.path), asset.ext));
        }

        expoUpdateManifest.metadata = {
            otaVersion: exp.extra.otaVersion,
            platform
        };

        // For Expo Go
        expoUpdateManifest.extra = {
            expoClient: exp,
            scopeKey: "@jottocraft/train"
        };

        console.log(platform + " manifest ID is " + expoUpdateManifest.id);

        fs.writeFileSync(path.join(OUTPUT_DIR, expoUpdateManifest.id + ".json"), JSON.stringify(expoUpdateManifest));
    }
});