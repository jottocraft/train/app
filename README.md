![Caltrain Live logo](https://i.imgur.com/T0PHvch.png)

This repository contains the source code for the Caltrain Live mobile app, written with Expo for Android and iOS.

### Building the app

- Node.JS LTS and yarn (`corepack enable`) is required.
- `yarn install` to install dependencies.
- `yarn run images` to optimize image assets.
- `yarn start` to start the Expo bundler (for streaming changes to debug builds, not needed for release builds).

#### Build the Android client

- Android SDK is required.
- `yarn run android-env:(debug|alpha|beta|release)` to generate a `env.bat` and `env.sh` that contains environment variables for building Caltrain Live. Run `env.bat` or `source env.sh`, and ensure all subsequent actions are performed in the same shell session to apply the configuration. If you want to use Android Studio, that must also be launched from a prepared shell to ensure autolinking doesn't crash.
- `yarn run android-prebuild` to generate an Android project in `/android`.
- Build/Run the generated project from the CLI (`cd android && gradlew (installDebug|assembleDebug|bundleRelease)`) or Android Studio (`studio64` if it's in your PATH)

#### Build the iOS client

- XCode and its command line tools and Cocoapods is required.
- `rm -rf ios` to clean out the ios folder (`ci_scripts` is only needed to setup XCode Cloud CI). Do not commit this deletion to git.
- `yarn run ios-env:(debug|alpha|beta|release)` to generate a `env.sh` that contains environment variables for building Caltrain Live. Run `source env.sh` and ensure all subsequent actions are performed in the same shell session to apply the configuration.
- `yarn run ios-prebuild` to generate an XCode project in `/ios`.
- Build/Run the generated project from XCode.

### Resources

- [Source code](https://gitlab.com/jottocraft/train)
- [Main website](https://train.jottocraft.com)
- [App on Google Play](https://play.google.com/store/apps/details?id=com.jottocraft.train)
- [App on the Apple App Store](https://apps.apple.com/us/app/caltrain-live/id6474943481)