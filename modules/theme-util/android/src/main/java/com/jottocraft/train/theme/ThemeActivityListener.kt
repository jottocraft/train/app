package com.jottocraft.train.theme

import android.app.Activity
import android.os.Bundle
import expo.modules.core.interfaces.ReactActivityLifecycleListener

import com.google.android.material.color.DynamicColors

class ThemeActivityListener : ReactActivityLifecycleListener {
  override fun onCreate(activity: Activity, savedInstanceState: Bundle?) {
    DynamicColors.applyToActivityIfAvailable(activity)
  }
}
