package com.jottocraft.train.theme

import expo.modules.kotlin.modules.Module
import expo.modules.kotlin.modules.ModuleDefinition

import android.R.color
import android.os.Build
import androidx.annotation.RequiresApi

class ThemeUtil : Module() {
  @RequiresApi(Build.VERSION_CODES.S)
  override fun definition() = ModuleDefinition {
    Name("ThemeUtil")

    Function("getThemeAccentColor") { dark: Boolean, shade: Int ->
      getThemeAccentColor(dark, shade)
    }

    Function("getThemeUIColor") { dark: Boolean, endpoint: Int, darkLightColors: Boolean ->
      getThemeUIColor(dark, endpoint, darkLightColors)
    }

    Function("getThemeTextColor") { dark: Boolean, endpoint: Int ->
      getThemeTextColor(dark, endpoint)
    }
  }

  private val context
    get() = requireNotNull(appContext.reactContext)

  @RequiresApi(Build.VERSION_CODES.S)
  public fun getThemeAccentColor(dark: Boolean, shade: Int): String? {
    // Light mode
    if (!dark && (shade == 0)) return hex(color.system_accent1_800);
    if (!dark && (shade == 1)) return hex(color.system_accent1_800);
    if (!dark && (shade == 2)) return hex(color.system_accent1_10);

    // Dark mode
    if (dark && (shade == 0)) return hex(color.system_accent1_200);
    if (dark && (shade == 1)) return hex(color.system_accent1_200);
    if (dark && (shade == 2)) return hex(color.system_accent1_900);

    return null;
  }

  @RequiresApi(Build.VERSION_CODES.S)
  public fun getThemeUIColor(dark: Boolean, endpoint: Int, darkLightColors: Boolean): String? {
    // Light mode
    if (darkLightColors) {
      if (!dark && (endpoint == 0)) return hex(color.system_neutral1_50);
      if (!dark && (endpoint == 1)) return hex(color.system_neutral1_400);
    } else {
      if (!dark && (endpoint == 0)) return hex(color.system_neutral1_10);
      if (!dark && (endpoint == 1)) return hex(color.system_neutral1_300);
    }

    // Dark mode
    if (dark && (endpoint == 0)) return hex(color.system_neutral1_900);
    if (dark && (endpoint == 1)) return hex(color.system_neutral1_600);

    return null;
  }

  @RequiresApi(Build.VERSION_CODES.S)
  public fun getThemeTextColor(dark: Boolean, endpoint: Int): String? {
    // Light mode
    if (!dark && (endpoint == 0)) return hex(color.system_neutral1_900);
    if (!dark && (endpoint == 1)) return hex(color.system_neutral1_600);

    // Dark mode
    if (dark && (endpoint == 0)) return hex(color.system_neutral1_100);
    if (dark && (endpoint == 1)) return hex(color.system_neutral1_400);

    return null;
  }

  @RequiresApi(Build.VERSION_CODES.S)
  private fun hex(colorID: Int): String {
    val androidColor = context.getColor(colorID)
    return String.format("#%06X", (0xFFFFFF and androidColor))
  }
}
