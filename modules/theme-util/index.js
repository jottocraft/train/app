const { requireNativeModule } = require("expo-modules-core");

let maybeThemeUtil = null;
try {
  maybeThemeUtil = requireNativeModule("ThemeUtil");
} catch(e) {}

module.exports = maybeThemeUtil;