const OTA_CHANNELS = require("./channels");

const CURRENT_VERSION = {
  // At least one of these three should change for new native builds
  MAJOR: 8,  // 1 digit max
  MINOR: 1,  // 2 digits max
  BUILD: 1,  // 2 digits max (resets to 0 when runtime version MAJOR.MINOR changes)

  OTA: 1     // 3 digits max (resets to 0 when runtime version MAJOR.MINOR changes)
};

module.exports = () => {
  const channelValue = process.env.TRAIN_CHANNEL_NAME ? OTA_CHANNELS[process.env.TRAIN_CHANNEL_NAME].value : 0;
  const isNativeBuild = process.env.TRAIN_NATIVE_BUILD === "true";

  // OTA builds don't have a channel, native build number, or native build version

  const versionName = isNativeBuild ? (
    `${CURRENT_VERSION.MAJOR}.${CURRENT_VERSION.MINOR}.${CURRENT_VERSION.BUILD}-${CURRENT_VERSION.OTA}${process.env.TRAIN_VERSION_NAME_SUFFIX ?? (process.env.TRAIN_CHANNEL_NAME ? OTA_CHANNELS[process.env.TRAIN_CHANNEL_NAME].suffix : "")}`
  ) : (
    `${CURRENT_VERSION.MAJOR}.${CURRENT_VERSION.MINOR}-${CURRENT_VERSION.OTA}`
  );

  const config = {
    expo: {
      name: process.env.TRAIN_APP_NAME ?? "Caltrain Live",
      nativeProjectName: "Train",
      description: "See detailed stats for every Caltrain in real-time.",
      // Only needed for Expo Go
      owner: "jottocraft",
      slug: "train",
      version: versionName,
      runtimeVersion: `${CURRENT_VERSION.MAJOR}.${CURRENT_VERSION.MINOR}`,
      jsEngine: "hermes",
      platforms: [
        "android",
        "ios"
      ],
      icon: "./assets/images/icon.png",
      userInterfaceStyle: "automatic",
      backgroundColor: "#282828",
      splash: {
        image: "./assets/images/splash.png",
        backgroundColor: "#282828",
        resizeMode: "contain"
      },
      assetBundlePatterns: [
        "**/*"
      ],
      ios: {
        supportsTablet: true,
        bundleIdentifier: "com.jottocraft.train" + (process.env.TRAIN_APP_ID_SUFFIX ?? ""),
        config: {
          usesNonExemptEncryption: false
        },
        appStoreUrl: "https://apps.apple.com/us/app/caltrain-live/id6474943481"
      },
      androidStatusBar: {
        barStyle: "light-content",
        backgroundColor: "#282828",
        translucent: false
      },
      androidNavigationBar: {
        barStyle: "light-content",
        backgroundColor: "#282828"
      },
      developmentClient: {
        silentLaunch: true
      },
      android: {
        adaptiveIcon: {
          foregroundImage: "./assets/images/ic_launcher_foreground.png",
          monochromeImage: "./assets/images/ic_launcher_monochrome.png",
          backgroundImage: "./assets/images/ic_launcher_background.png"
        },
        package: "com.jottocraft.train" + (process.env.TRAIN_APP_ID_SUFFIX ?? ""),
        playStoreUrl: "https://play.google.com/store/apps/details?id=com.jottocraft.train"
      },
      web: {
        favicon: "./assets/images/favicon.png",
        lang: "en-US"
      },
      plugins: [
        "sentry-expo",
        "expo-localization",
        "expo-build-properties",
        ["expo-font", {
          fonts: [
            "assets/fonts/PublicSans-Italic.ttf",
            "assets/fonts/PublicSans-Light.ttf",
            "assets/fonts/PublicSans-Regular.ttf",
            "assets/fonts/PublicSans-Medium.ttf",
            "assets/fonts/PublicSans-SemiBold.ttf",
            "assets/fonts/PublicSans-Bold.ttf",
            "assets/fonts/RobotoMono-Bold.ttf",
            "assets/fonts/MaterialSymbolsRounded24pt-Regular.ttf"
          ]
        }],
        "./plugins/withAndroidMaterial3",
        "./plugins/withXcodeDebugConfig",
        "./plugins/withXcodeProductName"
      ],
      updates: {
        enabled: true,
        url: "https://aero.jottocraft.com/train/expo/protocol1"
      }
    }
  };

  if (isNativeBuild) {
    const buildNumber = CURRENT_VERSION.MAJOR * Math.pow(10, 8) + CURRENT_VERSION.MINOR * Math.pow(10, 6) + CURRENT_VERSION.BUILD * Math.pow(10, 4) + CURRENT_VERSION.OTA * 10 + channelValue;
    config.expo.android.versionCode = buildNumber;
    config.expo.ios.buildNumber = String(buildNumber); // This doesn't actually matter since XCode Cloud always uses build numbers corresponding to the CI pipeline number

    // iOS uses Apple-mandated version string
    config.expo.version = process.env.TRAIN_IOS === "true" ? (
      `${CURRENT_VERSION.MAJOR}.${CURRENT_VERSION.MINOR}.${CURRENT_VERSION.BUILD * Math.pow(10, 4) + CURRENT_VERSION.OTA * 10 + channelValue}`
    ) : config.expo.version;

    config.expo.updates.requestHeaders = {
      // Aero will persist these values between updates
      "Expo-Channel-Name": process.env.TRAIN_CHANNEL_NAME,
      "jottocraft-Native-App-Version": btoa(JSON.stringify(CURRENT_VERSION)) // Expo's JSON parser crashes for some reason if this is left as a JSON string
    };
  } else {
    config.expo.extra = {
      otaVersion: CURRENT_VERSION.OTA
    };
  }

  return config;
}