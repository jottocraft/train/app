module.exports = {
    staging: { suffix: "s", value: 4 },
    techpreview: { suffix: "p", value: 3 },
    alpha: { suffix: "a", value: 2 },
    beta: { suffix: "b", value: 1 },
    production: { suffix: "", value: 0 }
};