/**
 * Sets environment variables from JSON for native project codegen
 */

const fs = require("fs");
const path = require("path");

if (!process.env.TRAIN_BUILD_MODE) {
    throw new Error("TRAIN_BUILD_MODE not set");
}

console.log("Collecting environment variables for Caltrain Live " + process.env.TRAIN_BUILD_MODE);

const eas = JSON.parse(fs.readFileSync(path.join(__dirname, "env.json")).toString());

const env = eas.build[process.env.TRAIN_BUILD_MODE].env;
if (process.argv.includes("--ios")) env["TRAIN_IOS"] = "true";

if (process.argv.includes("--verify")) {
    for (const env_var in env) {
        if (process.env[env_var] !== env[env_var]) {
            throw new Error(`Caltrain Live CI is misconfigured; expected ${env_var}=${env[env_var]}, got ${process.env[env_var]}`);
        }
    }

    console.log("Caltrain Live build environment verified successfully.");
} else {
    env.TRAIN_BUILD_MODE = process.env.TRAIN_BUILD_MODE;

    let envbat = "@echo off\r\nchcp 65001\r\n", envsh = "#!/bin/sh\n";
    for (const env_var in env) {
        envbat += `set ${env_var}=${env[env_var]}\r\n`;
        envsh += `export ${env_var}="${env[env_var]}"\n`;
    }

    envbat += `title Caltrain Live ${env.TRAIN_BUILD_MODE.toUpperCase()} Build Environment`;

    fs.writeFileSync(path.join(__dirname, "env.bat"), envbat);
    fs.writeFileSync(path.join(__dirname, "env.sh"), envsh);

    console.log("Wrote environment variables to env.bat and env.sh");
}