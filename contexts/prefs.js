import React, { createContext, useState, useEffect, useCallback, useMemo, useContext } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const prefs = {
    //Display
    theme: "system", //theme name
    showSingleTracking: true, //shows single tracking and other events on track
    trivialBackButtons: false, // shows trivial in-app back buttons on Android

    //Trains
    keepTimetableSFAtTop: true, //sets timetable direction in train view to show SF at the top
    showEquipmentImages: true, //shows equipment images in the train screen

    //Timetable
    delayPerStop: true, //shows delays for each stop in train view
    showSeconds: false, //shows seconds in train view
    showStopBypasses: false, //shows stop bypasses in the train timetable
    autoScrollToNextStop: true, //scrolls to the next stop when opening a train timetable

    //Implicit Preferences
    timetableOrigin: "San Francisco", //trip planner origin
    timetableDest: "San Jose Diridon", //trip planner dest
    readAlerts: [], //An array of alert IDs that have been read by the user
    selectedStream: "real", //The data stream chosen by the user
    userTrains: {}, //An object of train IDs important to the user and information about their saved status and alerts

    //Analytics
    iid: null,

    //Hidden features
    dev: __DEV__,
    debugLayout: false,
    debugRealtimeError: false,
    debugUseArrBypass: false,
    debugAllAlerts: false,
    debugRealtimeDevelopment: false,
    debugDarkAdaptiveColor: false,
    debugForceAccentColor: false,
    debugBigScreenUX: false
};

//Maps old -> new theme IDs
const themeUpgrades = {
    black: "midnight",
    ocean: "typhoon",
    beige: "antique"
};

const prefCtx = Object.keys(prefs).reduce((acc, cv) => {
    acc[cv] = createContext(prefs[cv]);
    return acc;
}, {});

export default function usePref(k) {
    return useContext(prefCtx[k]);
}

export const RootPrefStore = ({ children }) => {    
    return Object.keys(prefs).reduce((acc, cv) => {
        const ctx = prefCtx[cv];
        return <PrefStore name={cv} def={prefs[cv]} ctx={ctx}>{acc}</PrefStore>;
    }, children);
};

export const PrefStore = ({ name, def, ctx, children }) => {
    const [thisPref, setThisPref] = useState(def);

    useMemo(() => {
        AsyncStorage.getItem(name).then(value => {
            if (value !== null) {
                var parsedValue = value;

                if (value == "false") parsedValue = false;
                if (value == "true") parsedValue = true;
                if (value && (value.startsWith("[") || value.startsWith("{"))) try { parsedValue = JSON.parse(value); } catch (e) { }

                if (name === "theme") {
                    //Seamlessly upgrade old theme IDs to new theme IDs
                    if (themeUpgrades[parsedValue]) parsedValue = themeUpgrades[parsedValue];
                    AsyncStorage.setItem("theme", String(parsedValue));
                }

                setThisPref(parsedValue);
            }
        });
    }, []);

    function update(value) {
        AsyncStorage.setItem(name, typeof value == "object" ? JSON.stringify(value) : String(value));
        setThisPref(value);
    }
    
    function toggle() {
        update(!thisPref);
    }

    return (
        <ctx.Provider value={{ val: thisPref, update, toggle }}>
            {children}
        </ctx.Provider>
    );
};