import React, { createContext, useCallback, useRef, useState } from 'react';
import { Platform } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

export const screensContext = createContext(null);
export const safeAreaContext = createContext(null);

export const ScreensStore = ({ children }) => {
    const screenTitlesRef = useRef({});
    const [screenTitlesState, setScreenTitlesState] = useState(screenTitlesRef.current);
    const setScreenTitles = useCallback((v) => {
        screenTitlesRef.current = v;
        setScreenTitlesState(v);
    }, [screenTitlesRef, setScreenTitlesState]);

    let safeArea = { top: 0, right: 0, bottom: 0, left: 0 };
    if (Platform.OS === "ios") safeArea = useSafeAreaInsets();

    return (
        <safeAreaContext.Provider value={safeArea}>
            <screensContext.Provider value={{ screenTitlesState, screenTitlesRef, setScreenTitles }}>
                {children}
            </screensContext.Provider>
        </safeAreaContext.Provider>
    )
};