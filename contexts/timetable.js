import React, { createContext, useState, useEffect, useMemo, useContext, useCallback } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

import bundledTimetable from '../data/timetable.liveasset';
import { initTimetable } from '../utils/initTimetable';
import useRealtimeData from "../utils/realtime";
import usePref from './prefs';

const alertContext = createContext(null);
const streamContext = createContext(null);
const timetableContext = createContext(null);

export { alertContext, timetableContext, streamContext };
export const TimetableStore = ({ children }) => {
    const selectedStream = usePref("selectedStream");

    //Load timetable
    const [timetable, setTimetable] = useState(null);
    useMemo(() => {
        console.log("Loading installed timetable");
        AsyncStorage.getItem("timetable").then(value => {
            //Find appropriate timetable
            return new Promise(r => {
                if (value !== null) {
                    try {
                        const storedTimetable = JSON.parse(value);

                        if (storedTimetable.liveasset.ver > bundledTimetable.liveasset.ver) {
                            //Stored timetable is newer
                            console.log("Using installed timetable");
                            return r(storedTimetable);
                        } else {
                            //Bundled timetable is newer (probably because of Caltrain live play store update)
                            console.log("Deleting installed timetable (bundled is newer)");
                            AsyncStorage.removeItem("timetable");
                            return r(bundledTimetable);
                        }
                    } catch (e) {
                        console.warn("Invalid installed timetable", e);
                        return r(bundledTimetable);
                    }
                } else {
                    console.log("No installed timetable; using bundled");
                    return r(bundledTimetable);
                }
            });
        }).then(newTimetable => {
            //Process and apply timetable
            newTimetable = initTimetable(newTimetable);

            setTimetable(newTimetable);
        });
    }, []);

    //Realtime data function
    const realtimeData = useRealtimeData(timetable, setTimetable);

    const [alerts, setAlerts] = useState({});
    useEffect(() => {
        if (!realtimeData.result) return;

        console.log("Updating realtime patch");
        if (realtimeData.result?.timetable) setTimetable({ ...realtimeData.result.timetable });
        setAlerts({ realtimeResult: true, ac: new Date().getTime(), ao: realtimeData.result.ao, warning: realtimeData.result.warning, timetableUpdate: realtimeData.result.timetableUpdate, ...realtimeData.result?.alerts });
    }, [realtimeData.result, alerts.realtimeResult]);

    //Data stream
    const stream = useMemo(() => {
        if (selectedStream.val == "real") {
            return {
                text: alerts.realtimeResult ? (alerts.realtime ? "No data" : "Realtime") : "Loading...",
                hasData: alerts.realtimeResult && !alerts.realtime,
                icon: alerts.realtimeResult ? (alerts.realtime ? "leak_remove" : "leak_add") : "conversion_path",
                blink: alerts.realtimeResult ? false : true,
                id: "real",
                color: alerts.realtimeResult ? (alerts.realtime ? "errorAlert" : "real") : "loading"
            };
        } else if (selectedStream.val == "scheduled") {
            return {
                text: "Scheduled",
                hasData: true,
                icon: "sync_disabled",
                id: "scheduled",
                color: "scheduled"
            };
        }
    }, [selectedStream, alerts.realtime, alerts.realtimeResult]);

    //Pass update functions
    if (alerts) alerts.refresh = realtimeData.canUseRealtime && realtimeData.updateRealtime;

    console.log("TTContextUpdate");

    return (
        <timetableContext.Provider value={timetable}>
            <alertContext.Provider value={alerts}>
                <streamContext.Provider value={stream}>
                    {children}
                </streamContext.Provider>
            </alertContext.Provider>
        </timetableContext.Provider>
    )
};