import React, { createContext, useContext, useState, useMemo, useEffect } from 'react';
import { useColorScheme, Platform, PlatformColor, useWindowDimensions } from 'react-native';
import chroma from "chroma-js";

import { getThemeColors } from "shamrock-ux/lib/core/themes";
import { jottocraftThemeEnv } from "shamrock-ux/lib/core/jottocraft";

import ThemeUtil from "../modules/theme-util";

import { timetableContext } from "./timetable";
import usePref from './prefs';

function getSystemColorPatch(system, dark, darkLightColors, forceAccentColor) {
    if (Platform.OS === "windows") {
        //Use Windows accent
        return {
            accent: {
                element: {
                    ui: dark ? PlatformColor('SystemAccentColorLight2') : PlatformColor('SystemAccentColorDark2'),
                    type: dark ? PlatformColor('SystemAccentColorDark3') : "#ededed"
                },
                type: dark ? PlatformColor('SystemAccentColorLight3') : PlatformColor('SystemAccentColorDark3')
            }
        };
    } else if (Platform.OS === "android") {
        const ui = {}, type = {};
        let accent = null

        if ((Platform.Version >= 31) && ThemeUtil && (system || forceAccentColor)) {
            //Use dynamic accent color on Android 12+
            accent = {
                element: {
                    ui: ThemeUtil.getThemeAccentColor(dark, 0),
                    type: ThemeUtil.getThemeAccentColor(dark, 2)
                },
                type: ThemeUtil.getThemeAccentColor(dark, 1)
            }
        }

        //Use full dynamic colors on system theme Android 12+
        if ((Platform.Version >= 31) && ThemeUtil && system) {
            //Calculate Shamrock UI colors
            const uiScale = chroma.bezier([ThemeUtil.getThemeUIColor(dark, 0, darkLightColors), ThemeUtil.getThemeUIColor(dark, 1, darkLightColors)]).scale().correctLightness().mode('lch').colors((7 * 2) + 1);

            for (var i = 0; i < uiScale.length; i++) {
                ui[i * 5] = uiScale[i];
            }

            //Calculate Shamrock Type colors
            const typeScale = chroma.bezier([ThemeUtil.getThemeTextColor(dark, 0), ThemeUtil.getThemeTextColor(dark, 1)]).scale().correctLightness().mode('lch').colors((10 * 1) + 1);

            for (var i = 0; i < typeScale.length; i++) {
                type[i * 5] = typeScale[i];
            }
        }

        return {
            ui,
            type,
            accent
        };
    }

    return {};
}

function flattenCustomColors(theme, colors) {
    if (!colors || (typeof colors !== "object")) return colors;
    if (colors.light && colors.dark) {
        return theme.dark ? colors.dark : colors.light
    } else {
        return Object.keys(colors).reduce((acc, cv) => {
            acc[cv] = flattenCustomColors(theme, colors[cv]);
            return acc;
        }, {});
    }
}

const themeContext = createContext(null);

export default themeContext;
export const ThemeStore = ({ children }) => {
    const themePref = usePref("theme");
    const debugDarkAdaptiveColor = usePref("debugDarkAdaptiveColor");
    const debugForceAccentColor = usePref("debugForceAccentColor");
    const debugBigScreenUX = usePref("debugBigScreenUX");

    const { width } = useWindowDimensions();

    const service = useContext(timetableContext)?.service;
    const systemTheme = useColorScheme();

    const themeColors = useMemo(() => {
        var appliedThemeID = themePref.val ?? "dark";
        if ((themePref.val === "system") && systemTheme) appliedThemeID = systemTheme;

        //The seconds argument (site theme) should be removed in a future shamrock-ux update
        //that fully removes legacy site themes in favor of inheritable site and user envs
        const computedTheme = getThemeColors(appliedThemeID, {}, {
            site: {
                env: {
                    styles: {
                        light: {
                            accent: {
                                element: {
                                    ui: "#b12c2c",
                                    type: "#ededed"
                                },
                                type: "#ac3131"
                            }
                        },
                        dark: {
                            accent: {
                                element: {
                                    ui: "#ff7575",
                                    type: "#410000"
                                },
                                type: "#ff8888"
                            }
                        }
                    }
                },
                upstream: jottocraftThemeEnv?.site
            }
        });

        //Apply eligible system patch colors to base theme
        const systemPatch = getSystemColorPatch(themePref.val === "system", computedTheme.dark, !debugDarkAdaptiveColor.val, debugForceAccentColor.val);
        if (systemPatch.type) Object.assign(computedTheme.type, systemPatch.type);
        if (systemPatch.ui) Object.assign(computedTheme.ui, systemPatch.ui);
        if (systemPatch.accent) computedTheme.accent = systemPatch.accent;

        //Add Caltrain service colors
        if (service?.regular?.lineColors) Object.assign(computedTheme, flattenCustomColors(computedTheme, service.regular.lineColors));

        //Add extra Caltrain Live UI colors
        Object.assign(computedTheme, flattenCustomColors(computedTheme, {
            signType: {
                light: "#b45309",
                dark: "#fdba74"
            },
            streamType: {
                real: {
                    light: "#1577c2",
                    dark: "#47afff"
                },
                dataWarning: {
                    light: "#f59e0b",
                    dark: "#fbbf24"
                },
                errorAlert: {
                    light: "#c71818",
                    dark: "#ff5b5b"
                },
                loading: {
                    light: computedTheme.type[100],
                    dark: computedTheme.type[100]  
                },
                trainOffline: computedTheme.type[30],
                scheduled: computedTheme.type[80]
            }
        }));

        Object.assign(computedTheme, {
            font: {
                regular: "PublicSans-Regular",
                bold: "PublicSans-Bold",
                italic: "PublicSans-Italic",
                light: "PublicSans-Light",
                medium: "PublicSans-Medium",
                semibold: "PublicSans-SemiBold",
                monoBold: "RobotoMono-Bold",
            }
        });

        console.log("Theme update", themePref.val, "->", computedTheme.id, computedTheme);

        return computedTheme;
    }, [themePref, service?.regular?.lineColors, debugDarkAdaptiveColor, debugForceAccentColor, systemTheme]);

    const theme = useMemo(() => {
        let isBigScreen = width >= 950;
        if (debugBigScreenUX.val) isBigScreen = !isBigScreen;
        const isReallyBigScreen = width >= 1000;
        return {
            ...themeColors,
            bigScreen: isBigScreen,
            screenHorizPadding: isReallyBigScreen ? 30 : 15
        };
    }, [themeColors, debugBigScreenUX, width]);

    return (
        <themeContext.Provider value={theme}>
            {children}
        </themeContext.Provider>
    )
};