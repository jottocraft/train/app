#!/bin/sh

set -e

echo "Preparing enviornment..."
cd $CI_PRIMARY_REPOSITORY_PATH
CI="true" # Expo doesn't like it when this is set to TRUE

echo "Installing Node.JS..."
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
nvm install --lts

echo "Preparing Yarn..."
corepack enable

echo "Verifying Caltrain Live configuration..."
node env.js --ios --verify

echo "Installing JavaScript modules..."
yarn install --immutable

echo "Optimizing images..."
yarn run images

echo "Installing CocoaPods..."
HOMEBREW_NO_AUTO_UPDATE="1"
brew install cocoapods

echo "Generating XCode project..."
rm -rf ios
yarn run ios-prebuild

# https://github.com/facebook/react-native/issues/43285
echo "Fix incorrectly-generated XCode env..."
rm ios/.xcode.env.local
echo export NODE_BINARY=$(command -v node) > ios/.xcode.env.local

exit 0