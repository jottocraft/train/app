import React, { useContext, useEffect, useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";

import themeContext from "../contexts/theme";
import { alertContext, streamContext, timetableContext } from "../contexts/timetable";
import usePref from "../contexts/prefs";
import { useAsideRoute } from "./Screen";
import EmptyFeedback from "./EmptyFeedback";
import RoundIcon from "./RoundIcon";
import TouchEffect from "./TouchEffect";

const STATION_DISTANCE_MULTIPLIER = 40;
const END_OF_LINE_FUDGE = 50;

export default function Tracks() {
    const theme = useContext(themeContext);
    const timetable = useContext(timetableContext);
    const stream = useContext(streamContext);
    const alerts = useContext(alertContext);

    const track = "super";

    const trips = timetable.trips;
    const stops = timetable.tracks[track];

    const showSingleTracking = usePref("showSingleTracking");

    const { navigate } = useNavigation();
    const asideRoute = useAsideRoute();

    const [activeTrains, setActiveTrains] = useState(null);
    useEffect(() => {
        if (!trips || !stream?.id) return;

        function showTrains() {
            const activeTrains = [];
            const now = new Date().getTime();

            Object.keys(trips).forEach(id => {
                const train = trips[id];

                if (train.inService == true) {
                    for (var ii = 0; ii < (train.stops.length - 1); ii++) {
                        const stopTime = train.stops[ii], nextStopTime = train.stops[ii + 1];
                        const stop = stops.find(s => s.IDs.includes(stopTime.id)), nextStop = stops.find(s => s.IDs.includes(nextStopTime.id));

                        if (now > new Date(stopTime[stream.id].dep).getTime()) {
                            //Check if train is on the way to the next stop
                            if (now < new Date(nextStopTime[stream.id].arr).getTime()) {
                                //Train hasn't gotten to next station yet, meaning that stop and stops[ii + 1] are the start and end stops of this leg
                                var percentage = (now - (new Date(stopTime[stream.id].dep).getTime())) / ((new Date(nextStopTime[stream.id].arr)) - (new Date(stopTime[stream.id].dep).getTime()));
                                activeTrains.push({
                                    //Make sure to update single tracking function when changing marginTop formula
                                    distance: stop.distance + ((nextStop.distance - stop.distance) * percentage),
                                    line: train.line,
                                    id: train.id,
                                    pos: train.dir === "SB" ? "left" : "right"
                                });

                                //Begin processing next train
                                continue;
                            }

                            //Check if train is stopped at this stop
                            if (now > new Date(nextStopTime[stream.id].arr).getTime()) {
                                //train has already arrived at next station
                                if (now < new Date(nextStopTime[stream.id].dep).getTime()) {
                                    activeTrains.push({
                                        //Make sure to update single tracking function when changing marginTop formula
                                        distance: nextStop.distance,
                                        line: train.line,
                                        id: train.id,
                                        pos: train.dir === "SB" ? "left" : "right",
                                        stopped: true
                                    });

                                    //Begin processing next train
                                    continue;
                                }
                            }
                        }
                    }
                }
            });

            setActiveTrains(activeTrains);
        }

        showTrains();
        const trainTimeout = setInterval(showTrains, 1000);
        return () => clearInterval(trainTimeout);
    }, [timetable, stream?.id]);

    const [activeEvents, setActiveEvents] = useState([]);
    useEffect(() => {
        if (!alerts?.service) return;

        const events = [];
        for (const alert of alerts.service) {
            if ((alert.from !== "PADS") || !alert.for || !alert.text) continue;

            let match = null; // The track to show the construction icon on (i.e. the track that's closed)
            if (/all.*?trains.*?board.*?southbound.*?platform/gi.test(alert.text)) match = "right";
            if (/all.*?trains.*?board.*?northbound.*?platform/gi.test(alert.text)) match = "left";

            if (!match) continue;

            for (const stopIdentifier of alert.for) {
                const stop = stops.find(s => s.IDs.includes(stopIdentifier) || (s.name === stopIdentifier));
                if (!stop) {
                    console.warn("Could not find stop for single tracking event for stop", stopIdentifier);
                    continue;
                }

                events.push({
                    id: stop.name,
                    title: "Single Tracking",
                    description: stop.name,
                    icon: "construction",
                    alert: alert,
                    pos: match,
                    distance: stop.distance
                });
            }
        }

        setActiveEvents(events);
    }, [alerts, timetable]);

    return (
        <View style={{ marginTop: 12 }}>
            {timetable.expired ? (
                <EmptyFeedback title="Timetable expired" icon="timer_off">
                    The timetable version installed on your device is out-of-date and has expired. Please enable and connect to realtime data and/or update Caltrain Live to get the latest schedule.
                </EmptyFeedback>
            ) : activeTrains && !activeTrains.length ? (
                <EmptyFeedback title="No active trains">
                    There aren't any active trains right now. Once there are, they'll show up below.
                </EmptyFeedback>
            ) : null}

            <View style={{ paddingHorizontal: theme.screenHorizPadding }}>
                <View style={{ justifyContent: "center", alignItems: "stretch", flexDirection: "row", marginBottom: 20, marginTop: 40 }}>
                    <View style={{ position: "relative" }}>
                        {/*Trains*/}
                        <View style={{ position: "absolute", top: 0, left: 0, zIndex: 20 }}>
                            {activeTrains?.map((prop, key) => {
                                const active = asideRoute?.name === "Train" && asideRoute?.params?.id === prop.id;
                                return (
                                    <View style={{
                                        position: "absolute",
                                        //(Distance * Multiplier) - 1/2 Height + Fudge
                                        top: (prop.distance * STATION_DISTANCE_MULTIPLIER) - 22 + END_OF_LINE_FUDGE,
                                        left: prop.pos === "right" ? 56 : 0,
                                        zIndex: 40,
                                        overflow: "visible"
                                    }}>
                                        <View style={{
                                            position: "absolute",
                                            left: 8,
                                            alignItems: "center",
                                            justifyContent: "center",

                                            backgroundColor: theme.ui[10],

                                            width: 28,
                                            height: 28,

                                            borderBottomLeftRadius: 10,
                                            borderBottomRightRadius: 10,

                                            // This is needed because the icons aren't perfectly vertically centered.
                                            // By rotating instead of using a different icon, we can guarentee that
                                            // the spacing is the same on both sides.
                                            transform: prop.pos === "right" ? [{ rotate: "180deg" }] : [],

                                            ...(prop.pos === "right" ? {
                                                bottom: 40,
                                            } : {
                                                top: 40
                                            })
                                        }}>
                                            <RoundIcon name={"keyboard_arrow_down"} size={28} color={theme.lineUI[prop.line]} />
                                        </View>
                                        <TouchEffect disabled={active} invert={!prop.stopped} key={prop.id} onPress={() => { navigate("Train", { id: prop.id }) }}
                                            outerStyle={{
                                                width: 44,
                                                height: 44,
                                                borderRadius: 22,

                                                backgroundColor: prop.stopped ? theme.ui[10] : theme.lineUI[prop.line],

                                                ...(prop.stopped ? {} : {
                                                    shadowColor: "#000",
                                                    shadowOffset: {
                                                        width: 0,
                                                        height: 1,
                                                    },
                                                    shadowOpacity: 0.18,
                                                    shadowRadius: 1.00,
                                                    elevation: 1
                                                })
                                            }}
                                            innerStyle={{
                                                alignItems: "center",
                                                justifyContent: "center",

                                                ...(active ? {
                                                    borderRadius: 22,
                                                    borderWidth: 4,
                                                    borderColor: prop.stopped ? theme.ui[20] : (theme.lineUIText[prop.line] + "88")
                                                } : {})
                                            }}>
                                            <RoundIcon color={prop.stopped ? theme.lineType[prop.line] : theme.lineUIText[prop.line]} name={"train"} size={26} />
                                        </TouchEffect>
                                    </View>
                                );
                            })}
                            {/* Single tracking */}
                            {showSingleTracking.val ? activeEvents.map((prop, key) => {
                                const active = asideRoute?.name === "Event" && asideRoute?.params?.id === prop.id;
                                return (
                                    <View style={{
                                        position: "absolute",
                                        //(Distance * Multiplier) - 1/2 Height + Fudge
                                        top: (prop.distance * STATION_DISTANCE_MULTIPLIER) - 22 + END_OF_LINE_FUDGE,
                                        left: prop.pos === "right" ? 64 : 8,
                                        zIndex: 30,
                                        overflow: "visible"
                                    }}>
                                        <TouchEffect disabled={active} key={prop.id} onPress={() => { navigate("Event", { id: prop.id, title: prop.title, description: prop.description, icon: prop.icon, alert: prop.alert }) }}
                                            outerStyle={{
                                                width: 28,
                                                height: 44,
                                                borderRadius: 10,

                                                backgroundColor: theme.ui[20]
                                            }}
                                            innerStyle={{
                                                alignItems: "center",
                                                justifyContent: "center",

                                                ...(active ? {
                                                    borderRadius: 10,
                                                    borderWidth: 2,
                                                    borderColor: theme.ui[30]
                                                } : {})
                                            }}>
                                            <RoundIcon color={active ? theme.accent.type : theme.type[100]} name={prop.icon} size={22} />
                                        </TouchEffect>
                                    </View>
                                );
                            }) : null}
                        </View>
                        {/*Tracks*/}
                        <View style={{ position: "relative", zIndex: 10, justifyContent: "center", flexDirection: "row", flex: 1, paddingLeft: 8 }}>
                            {["left", "right"].map(pos => {
                                return (
                                    <View key={pos} style={{
                                        height: "100%",
                                        width: 28,
                                        backgroundColor: theme.ui[10],
                                        borderRadius: 10,
                                        alignItems: "center",
                                        marginLeft: pos === "right" ? 28 : 0,
                                        position: "relative",
                                        overflow: "hidden"
                                    }} />
                                );
                            })}
                        </View>
                    </View>

                    <View style={{ width: 24 }} />

                    {/*Stop labels*/}
                    <View style={{ position: "relative" }}>
                        {stops.map((stop, i) => {
                            const height = 30;

                            //margintop: (Stop milepost * multiplier (defined at top)) - (1/2 of height for centering)
                            const positionStyle = { marginTop: (stop.distancePrev * STATION_DISTANCE_MULTIPLIER) + (0 == i ? (-1 * height / 2) + END_OF_LINE_FUDGE : (-1 * height)), marginBottom: ((stops.length - 1) == i) ? END_OF_LINE_FUDGE : 0 };
                            const active = asideRoute?.name === "Station" && asideRoute?.params?.track === track && asideRoute?.params?.i === i;

                            return (
                                <TouchableOpacity
                                    style={[{
                                        flexDirection: "row",
                                        alignItems: "center",
                                        height: height,
                                        backgroundColor: active ? theme.ui[20] : undefined,
                                        paddingHorizontal: 10,
                                        borderRadius: 10
                                    }, positionStyle]}
                                    key={stop.name}
                                    disabled={active}
                                    onPress={() => { navigate("Station", { track, i }) }}
                                >
                                    <Text style={{ color: stop.inService ? theme.type[10] : theme.type[90], fontFamily: theme.font.medium, fontSize: 15 }}>{stop.name}</Text>
                                </TouchableOpacity>
                            );
                        })}
                    </View>
                </View>
            </View >
        </View>
    );
}