import React, { useContext } from "react";
import { View, TouchableNativeFeedback, TouchableHighlight, Platform } from "react-native";
import themeContext from "../contexts/theme";

function TouchEffectOuter({ wide, outerStyle, children }) {
    const theme = useContext(themeContext);
    const borderRadius = outerStyle?.borderRadius !== undefined ? outerStyle.borderRadius : (wide && theme.bigScreen ? 10 : 0);

    return (
        <View style={{ ...outerStyle, borderRadius, overflow: "hidden", ...(wide ? { marginHorizontal: theme.bigScreen ? theme.screenHorizPadding - 10 : 0 } : {}) }}>
            {children}
        </View>
    );
}

export default function TouchEffect({ children, wide, outerStyle, innerStyle, invert, ...props }) {
    const theme = useContext(themeContext);

    const innerContent = (
        <View style={{ flex: 1, paddingHorizontal: wide ? (theme.bigScreen ? 10 : theme.screenHorizPadding) : 0 }}>
            <View style={{ flex: 1, ...innerStyle }}>
                {children}
            </View>
        </View>
    );

    if (Platform.OS === "android") {
        return (
            <TouchEffectOuter wide={wide} outerStyle={outerStyle}>
                <TouchableNativeFeedback style={{ flex: 1 }} background={TouchableNativeFeedback.Ripple(invert ? theme.ui[0] : theme.ui[50])} {...props}>
                    {innerContent}
                </TouchableNativeFeedback>
            </TouchEffectOuter>
        );
    }

    if ((Platform.OS === "ios") || (Platform.OS === "web")) {
        return (
            <TouchEffectOuter wide={wide} outerStyle={outerStyle}>
                <TouchableHighlight style={{ flex: 1 }} underlayColor={invert ? theme.ui[0] + "88" : theme.ui[50] + "88"} {...props}>
                    {innerContent}
                </TouchableHighlight>
            </TouchEffectOuter>
        );
    }
}