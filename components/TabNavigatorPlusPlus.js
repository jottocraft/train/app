/**
 * Adapted from https://github.com/react-navigation/react-navigation/blob/main/packages/bottom-tabs/src/views/BottomTabView.tsx
 */

import React, { useContext, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { createNavigatorFactory, useNavigationBuilder, TabRouter } from '@react-navigation/native';
import { ScreenContainer, Screen } from "react-native-screens";

import TabBar from './TabBar';
import themeContext from '../contexts/theme';

function TabNavigatorPlusPlus({
    children,
    screenOptions,
    initialRouteName
}) {
    const { state, navigation, descriptors, NavigationContent } = useNavigationBuilder(TabRouter, {
        children,
        screenOptions,
        initialRouteName
    });

    const focusedRouteKey = state.routes[state.index].key;
    const [loaded, setLoaded] = useState([focusedRouteKey]);

    if (!loaded.includes(focusedRouteKey)) {
        setLoaded([...loaded, focusedRouteKey]);
    }

    const theme = useContext(themeContext);

    const tabBar = (
        <TabBar state={state} descriptors={descriptors} navigation={navigation} />
    );

    return (
        <NavigationContent>
            <View style={{ flex: 1, flexDirection: theme.bigScreen ? "row" : "column" }}>
                {theme.bigScreen ? tabBar : null}
                <View style={{ flex: 1, position: "realtive", zIndex: 10 }}>
                    <ScreenContainer enabled hasTwoStates style={{ flex: 1 }}>
                        {state.routes.map((route, i) => {
                            const descriptor = descriptors[route.key];
                            const isFocused = i === state.index;

                            if (!isFocused && !loaded.includes(route.key)) {
                                return null;
                            }

                            return (
                                <Screen
                                    key={route.key}
                                    style={StyleSheet.absoluteFill}
                                    activityState={isFocused ? 2 : 0}
                                    enabled
                                    freezeOnBlur
                                >
                                    {descriptor.render()}
                                </Screen>
                            );
                        })}
                    </ScreenContainer>
                </View>
                {!theme.bigScreen ? tabBar : null}
            </View>
        </NavigationContent>
    );
}

export const createTabNavigatorPlusPlus = createNavigatorFactory(TabNavigatorPlusPlus);