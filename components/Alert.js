import React, { useContext } from 'react';
import {
    Text,
    View
} from 'react-native';

import themeContext from '../contexts/theme';

import RoundIcon from './RoundIcon';
import usePref from '../contexts/prefs';

export var isAlertImportant = function (alert, readAlerts) {
    return ["511", "jottocraft"].includes(alert.from) && !readAlerts.includes(alert.id);
};

export default function Alert(alert) {
    const theme = useContext(themeContext);
    const readAlerts = usePref("readAlerts");

    const debugAllAlerts = usePref("debugAllAlerts");

    var fromBrand = {};
    if (alert.from === "511") {
        fromBrand = {
            icon: "warning",
            color: theme.dark ? "#FCD34D" : "#D97706"
        };
    } else if (alert.from === "PADS") {
        fromBrand = {
            icon: "campaign",
            communityIcon: true,
            color: theme.signType
        };
    } else if (alert.from === "jottocraft") {
        fromBrand = {
            icon: "rss_feed",
            communityIcon: true,
            color: theme.accent.type
        };
    }

    let alertDisplayTitle = alert.title || "Alert";
    if (alert.from === "PADS") alertDisplayTitle = alert.for ? (debugAllAlerts.val ? alert.for.join(", ") : "Station Announcement") : "System-Wide Announcement";

    let alertIcon = fromBrand.icon;
    if (typeof fromBrand.icon === "string") alertIcon = <RoundIcon name={fromBrand.icon} size={20} color={fromBrand.color} />;

    if (alert.mini) {
        return (
            <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 4 }}>
                <View style={{ alignItems: "center", width: 40, marginRight: 6 }}>
                    {alertIcon}
                </View>
                <Text style={{ fontFamily: theme.font.regular, color: fromBrand.color, fontSize: 14, flexShrink: 1 }} ellipsizeMode="tail" numberOfLines={1}>{alertDisplayTitle}</Text>
            </View>
        )
    }
    return (
        <View style={{
            borderRadius: 10,
            padding: 0,
            backgroundColor: theme.dark ? theme.ui[10] : theme.ui[0],
            marginHorizontal: 1,

            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 1,
            },
            shadowOpacity: 0.18,
            shadowRadius: 1.00,
            elevation: 1
        }}>
            <View style={{
                backgroundColor: theme.dark ? theme.ui[20] : theme.ui[10],
                borderTopStartRadius: 10,
                borderTopEndRadius: 10,
                padding: 8,
                flexDirection: "row",
                alignItems: "center"
            }}>
                {alertIcon}
                <View style={{ flexGrow: 1, flexShrink: 1, marginHorizontal: 5 }}>
                    <Text style={{ fontFamily: theme.font.medium, color: fromBrand.color, fontSize: 14 }}>{alertDisplayTitle}</Text>
                </View>
                {!alert.id ? (
                    <RoundIcon name="push_pin" size={16} color={theme.type[100]} />
                ) : (alert.from === "511") && readAlerts.val.includes(alert.id) ? (
                    <RoundIcon name="done" size={16} color={theme.type[100]} />
                ) : null}
            </View>
            <View style={{ padding: 10 }}>
                <Text style={{ fontFamily: theme.font.regular, color: theme.type[20], fontSize: 12 }}>{alert.text}</Text>
            </View>
        </View>
    );
}