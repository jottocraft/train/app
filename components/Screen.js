import React, { useContext, useEffect, useLayoutEffect, useState } from 'react';
import {
    Platform,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity
} from 'react-native';
import { useIsFocused, useNavigation, useNavigationState, useRoute } from '@react-navigation/native';

import RoundIcon from './RoundIcon';
import { safeAreaContext, screensContext } from "../contexts/screens";
import themeContext from '../contexts/theme';
import usePref from '../contexts/prefs';
import { asideContext } from './SplitStack';

export function useAsideRoute() {
    const { asideRef, asideIsReady } = useContext(asideContext);
    const theme = useContext(themeContext);

    const [route, setRoute] = useState((theme.bigScreen && asideRef?.isReady() && asideRef.getCurrentRoute()) ?? null);

    useEffect(() => {
        if (!theme.bigScreen || !asideIsReady) {
            setRoute(null);
            return;
        }

        function onStateChanged() {
            setRoute((theme.bigScreen && asideRef?.isReady() && asideRef.getCurrentRoute()) ?? null);
        }

        asideRef.addListener("state", onStateChanged);
        onStateChanged();
        return () => asideRef.removeListener("state", onStateChanged);
    }, [asideRef, asideIsReady, theme.bigScreen]);

    return route;
}

export default function Screen({ children, header, refreshControl, fullWidth, stickyHeaderIndices, onScroll, onContainerLayout, scrollEventThrottle, svRef, iosStatusBarBg }) {
    const theme = useContext(themeContext);
    const { screenTitlesState, screenTitlesRef, setScreenTitles } = useContext(screensContext);

    const safeArea = useContext(safeAreaContext);

    const trivialBackButtonsPref = usePref("trivialBackButtons");
    const showTrivialBackButtons = trivialBackButtonsPref.val || (Platform.OS === "ios") || (Platform.OS === "web");

    const navigation = useNavigation();
    const route = useRoute();
    const previousKey = useNavigationState(state => {
        if ((state.type === "tab") || !state.routes) return null;
        const thisScreenIndex = state.routes.findIndex(r => r.key === route.key);
        if (thisScreenIndex < 1) return null;
        return state.routes[thisScreenIndex - 1].key;
    });

    const showBackButton = (theme.bigScreen || showTrivialBackButtons) && previousKey && navigation.canGoBack();

    useEffect(() => {
        setScreenTitles({
            ...screenTitlesRef.current,
            [route.key]: header?.title
        });
        return () => {
            delete screenTitlesRef.current[route.key];
            setScreenTitles({ ...screenTitlesRef.current });
        };
    }, [header?.title, route.key, screenTitlesRef, setScreenTitles]);

    const isFocused = useIsFocused();
    useEffect(() => {
        if ((Platform.OS === "web") && header?.title && isFocused) document.title = `${header.title} - Caltrain Live`;
    }, [header?.title, isFocused]);

    return (
        <View style={[
            theme.bigScreen ? {
                paddingTop: safeArea.top,
                paddingRight: safeArea.right,
                paddingBottom: safeArea.bottom
            } : {
                paddingTop: safeArea.top,
                paddingRight: safeArea.right,
                paddingLeft: safeArea.left
            },
            { flex: 1, backgroundColor: iosStatusBarBg ?? theme.ui[0] }
        ]}>
            <View onLayout={onContainerLayout} style={{
                flex: 1
            }}>
                <ScrollView
                    ref={svRef}
                    scrollEventThrottle={scrollEventThrottle}
                    onScroll={onScroll}
                    style={{
                        paddingHorizontal: fullWidth ? 0 : theme.screenHorizPadding,
                        backgroundColor: theme.ui[0]
                    }}
                    stickyHeaderIndices={stickyHeaderIndices?.map(i => i + 1)}
                    refreshControl={refreshControl || null}
                    indicatorStyle={theme.dark ? "white" : "black"}
                >
                    <View style={{ paddingHorizontal: fullWidth ? theme.screenHorizPadding : 0 }}>
                        {showBackButton ? (
                            <TouchableOpacity onPress={() => navigation.goBack()} style={{
                                flexDirection: "row",
                                alignItems: "center",
                                paddingVertical: Platform.OS === "web" ? 20 : 10
                            }}>
                                <RoundIcon name="keyboard_arrow_left" size={20} color={theme.type[0]} />
                                <Text style={{ fontFamily: theme.font.regular, fontSize: 16, color: theme.type[0] }}>{screenTitlesState[previousKey] || "Back"}</Text>
                            </TouchableOpacity>
                        ) : null}
                        {header ? (
                            <View style={{ alignItems: 'center', marginBottom: 24, marginTop: 30 }}>
                                {header.image ? (
                                    <View style={{ width: "100%" }}>
                                        <View style={{ position: "relative", overflow: "hidden", borderRadius: 10 }}>
                                            <Image
                                                style={{ width: "100%", height: 120 }}
                                                resizeMode={header.imageFit || "cover"}
                                                source={header.image}
                                            />
                                            {header.imageCredits ? (
                                                <View style={{
                                                    position: "absolute", bottom: 0, right: 0,
                                                    paddingHorizontal: 4, paddingVertical: 1,
                                                    backgroundColor: theme.ui[0] + "cc",
                                                    borderTopLeftRadius: 8,
                                                    flexDirection: "row", alignItems: "center"
                                                }}>
                                                    <View style={{ marginRight: 1 }}><RoundIcon name="copyright" size={8} color={theme.type[60]} /></View>
                                                    <Text style={{ fontSize: 6, fontFamily: theme.font.regular, color: theme.type[60] }}>{header.imageCredits}</Text>
                                                </View>
                                            ) : null}
                                        </View>
                                    </View>
                                ) : null}
                                <View style={{ marginTop: showBackButton && !header.image ? 16 : 32, flexDirection: 'row', alignItems: 'center' }}>
                                    {header.icon ? <View style={{ marginRight: 5 }}><RoundIcon name={header.icon} color={theme.type[0]} size={28} /></View> : null}
                                    <Text style={{ fontFamily: theme.font.bold, fontSize: 28, color: theme.type[0] }}>{header.title}</Text>
                                </View>
                                {header.description ? (
                                    <Text style={[{ fontFamily: theme.font.medium, color: theme.type[40], marginTop: 5, marginBottom: 22 }]}>{header.description}</Text>
                                ) : ((header.tags && header.tags.filter(tag => tag.text).length) ? (
                                    <View style={{ flexDirection: "row", marginTop: 7 }}>
                                        {header.tags.filter(tag => tag.text).map((tag, index) => (
                                            <View key={tag.text} style={{
                                                backgroundColor: tag.bg,
                                                borderRadius: 5,
                                                paddingHorizontal: 6,
                                                paddingVertical: 3,
                                                marginHorizontal: 4
                                            }}>
                                                <Text
                                                    style={{
                                                        fontFamily: theme.font.medium,
                                                        color: tag.color
                                                    }}
                                                >
                                                    {tag.text}
                                                </Text>
                                            </View>
                                        ))}
                                    </View>
                                ) : null)}
                            </View>
                        ) : null}
                    </View>
                    {children}
                    <View style={{ marginBottom: 15 }} />
                </ScrollView >
            </View>
        </View>
    );
}