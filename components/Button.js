import React, { useContext } from 'react';
import {
    Text, View
} from 'react-native';
import RoundIcon from './RoundIcon';
import themeContext from '../contexts/theme';
import TouchEffect from './TouchEffect';

export default function Button({ small, icon, children, onPress, active, onLongPress }) {
    const theme = useContext(themeContext);

    return (
        <View style={{
            height: small ? 38 : 45,
            borderWidth: 1,
            borderColor: active ? theme.accent.element.ui : (theme.dark ? theme.ui[25] : theme.ui[10]),
            borderRadius: 8,
            backgroundColor: active ? theme.accent.element.ui : (theme.dark ? theme.ui[15] : theme.ui[5]),
            marginHorizontal: 1
        }}>
            <TouchEffect
                onPress={onPress}
                onLongPress={onLongPress}
                outerStyle={{
                    flex: 1,
                    borderRadius: 8
                }}
                innerStyle={{
                    paddingHorizontal: small ? 10 : 14,
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center"
                }}
            >
                <>
                    {icon ? <View style={{ marginRight: small ? 2 : 5 }}><RoundIcon color={active ? theme.accent.element.type : theme.type[10]} name={icon} size={small ? 18 : 22} /></View> : null}
                    <Text style={{ color: active ? theme.accent.element.type : theme.type[10], fontFamily: theme.font.medium, fontSize: 14 }}>
                        {children}
                    </Text>
                </>
            </TouchEffect>
        </View>
    );
}