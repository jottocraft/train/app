/**
 * Adapted from https://github.com/oblador/react-native-vector-icons/blob/master/lib/create-icon-set.js
 */

import React from 'react';
import { Text, View } from 'react-native';

import codepoints from "../assets/fonts/MaterialSymbolsRounded24pt-Regular.codepoints.json";

export default function RoundIcon({ name, size, color, children, shadowRadius, style, ...props }) {
    if (style) throw new Error("Style prop not allowed in RoundIcon");

    // These shenanigans are needed to keep icons perfectly sized and centered
    // See https://github.com/facebook/react-native/issues/29507
    return (
        <View style={{
            position: "relative",
            width: size,
            height: size
        }}>
            <Text allowFontScaling={false} selectable={false} style={{
                position: "absolute",
                top: -size,

                fontSize: size,
                color,
                width: size,
                height: size * 3,
                lineHeight: size * 3,

                ...(shadowRadius ? {
                    textShadowColor: color,
                    textShadowRadius: shadowRadius
                } : {}),

                fontFamily: "MaterialSymbolsRounded24pt-Regular",
                fontWeight: "normal",
                fontStyle: "normal"
            }} {...props}>{String.fromCodePoint(codepoints[name])}</Text>
        </View>
    );
}