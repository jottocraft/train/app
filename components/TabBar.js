import React, { useContext, useState, useRef, useEffect } from 'react';
import {
    Platform,
    Text,
    View
} from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import Animated, { useSharedValue, withTiming, useAnimatedStyle } from 'react-native-reanimated';

import themeContext from '../contexts/theme';
import { safeAreaContext } from '../contexts/screens';
import TouchEffect from './TouchEffect';
import RoundIcon from './RoundIcon';

function HorizontalTabWrapper({ sliderAnimation, visibleRoutes, children }) {
    const theme = useContext(themeContext);

    const sliderLeft = useAnimatedStyle(() => {
        return {
            left: sliderAnimation.value + "%"
        };
    });

    return (
        <View style={{
            padding: 8,
            height: 64
        }}>
            <View style={{ flexDirection: 'row', justifyContent: "center", alignItems: "center", height: "100%" }}>
                <Animated.View style={[sliderLeft, {
                    position: "absolute", width: (100 / visibleRoutes.length) + "%", height: "100%", backgroundColor: theme.dark ? theme.ui[20] : theme.ui[10], borderRadius: 10,
                    justifyContent: "flex-end"
                }]}>
                    <View style={{ height: 2, backgroundColor: theme.accent.element.ui, marginBottom: 6, marginHorizontal: 10, borderRadius: 1 }} />
                </Animated.View>
                {children}
            </View>
        </View>
    );
}

function HorizontalTab({ isFocused, onPress, options, index, label }) {
    const theme = useContext(themeContext);

    return (
        <View style={{ flex: 1, zIndex: 20, height: "100%" }}>
            <TouchEffect
                accessibilityRole="button"
                accessibilityStates={isFocused ? ['selected'] : []}
                accessibilityLabel={options.tabBarAccessibilityLabel}
                testID={options.tabBarTestID}
                onPress={onPress}
                key={index}
                outerStyle={{ height: "100%", borderRadius: 10 }}
                innerStyle={{ flexDirection: "row", alignItems: "center", justifyContent: "center" }}
            >
                <Text style={{ color: isFocused ? theme.type[0] : theme.type[60], fontFamily: isFocused ? theme.font.semibold : theme.font.medium, fontSize: 14 }}>
                    {label}
                </Text>
            </TouchEffect>
        </View>
    );
}

function VerticalTabWrapper({ sliderAnimation, visibleRoutes, children }) {
    const theme = useContext(themeContext);

    const sliderTop = useAnimatedStyle(() => {
        return {
            top: sliderAnimation.value + "%"
        };
    });

    return (
        <View style={{
            padding: 10,
            width: 100,
            height: "100%"
        }}>
            <View style={{ flexDirection: "column", justifyContent: "center", alignItems: "center", height: "100%" }}>
                <View style={{ position: "absolute", height: visibleRoutes.length * 100 }}>
                    <Animated.View style={[sliderTop, {
                        width: 90, height: 100, backgroundColor: theme.dark ? theme.ui[20] : theme.ui[10], borderRadius: 10
                    }]}>
                        <View style={{ width: 2, height: 80, backgroundColor: theme.accent.element.ui, marginLeft: 6, marginVertical: 10, borderRadius: 1 }} />
                    </Animated.View>
                </View>
                {children}
            </View>
        </View>
    );
}

function VerticalTab({ isFocused, onPress, options, index, label }) {
    const theme = useContext(themeContext);
    const color = isFocused ? theme.type[0] : theme.type[60];

    return (
        <View style={{ zIndex: 20, width: 90, height: 100 }}>
            <TouchEffect
                accessibilityRole="button"
                accessibilityStates={isFocused ? ['selected'] : []}
                accessibilityLabel={options.tabBarAccessibilityLabel}
                testID={options.tabBarTestID}
                onPress={onPress}
                key={index}
                outerStyle={{ height: "100%", borderRadius: 10 }}
            >
                <View style={{ height: "100%", flexDirection: "column", alignItems: "center", justifyContent: "center", paddingHorizontal: 14 }}>
                    <RoundIcon name={options.icon} size={28} color={color} />
                    <Text style={{ color, fontFamily: isFocused ? theme.font.semibold : theme.font.medium, fontSize: 12, marginTop: 8, textAlign: "center" }}>
                        {label}
                    </Text>
                </View>
            </TouchEffect>
        </View>
    );
}

export default function TabBar({ state, descriptors, navigation }) {
    const theme = useContext(themeContext);

    const safeArea = useContext(safeAreaContext);

    const visibleRoutes = state.routes.filter(r => !descriptors[r.key].options.hidden);
    const selectedIndex = visibleRoutes.findIndex(r => r.key == state.routes[state.index].key);

    const sliderAnimation = useSharedValue(0);
    useEffect(() => {
        sliderAnimation.value = withTiming(selectedIndex / visibleRoutes.length * 100, { duration: 150 });
    }, [sliderAnimation, selectedIndex, visibleRoutes.length]);

    const TabWrapper = theme.bigScreen ? VerticalTabWrapper : HorizontalTabWrapper;
    const Tab = theme.bigScreen ? VerticalTab : HorizontalTab;

    const shouldRoundCorners = theme.bigScreen && ((Platform.OS === "ios") || theme.dark);

    return (
        <View
            style={[theme.bigScreen ? {
                marginTop: safeArea.top,
                marginBottom: safeArea.bottom,
                marginLeft: safeArea.left
            } : {
                paddingBottom: safeArea.bottom,
                paddingLeft: safeArea.left,
                paddingRight: safeArea.right
            }, {
                backgroundColor: theme.dark ? theme.ui[10] : theme.ui[0],
                position: "relative",
                zIndex: 20,

                // The navbar and status bar obstruct the shadow
                borderTopRightRadius: shouldRoundCorners ? 10 : 0,
                borderBottomRightRadius: shouldRoundCorners ? 10 : 0
            }, theme.dark && theme.bigScreen ? {} : (
                theme.bigScreen ? {
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 1,
                    },
                    shadowOpacity: 0.20,
                    shadowRadius: 1.41,
                    elevation: 10
                } : {
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 5,
                    },
                    shadowOpacity: 0.34,
                    shadowRadius: 6.27,
                    elevation: 10
                }
            )]}>
            <TabWrapper sliderAnimation={sliderAnimation} visibleRoutes={visibleRoutes}>
                {state.routes.map((route, index) => {
                    const { options } = descriptors[route.key];

                    const isFocused = state.index === index;

                    const onPress = () => {
                        const event = navigation.emit({
                            type: 'tabPress',
                            target: route.key,
                        });

                        if (!isFocused && !event.defaultPrevented) {
                            navigation.navigate(route.name);
                        }
                    };

                    return options.hidden ? null : (
                        <Tab key={route.key} isFocused={isFocused} onPress={onPress} options={options} index={index} label={options.label} />
                    );
                })}
            </TabWrapper>
        </View>
    );
}