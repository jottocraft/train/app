import React, { useContext, useRef, useMemo } from "react";
import {
    View,
    TouchableOpacity,
    Text
} from "react-native";
import RoundIcon from "./RoundIcon";
import themeContext from "../contexts/theme";
import usePref from "../contexts/prefs";
import Slider from "./Slider";
import TouchEffect from "./TouchEffect";

export default function Setting({ pref, name }) {
    const theme = useContext(themeContext);
    const state = usePref(name);

    const active = pref.disabled ? false : state.val;

    const numVerticalItems = pref.opts?.length ? pref.opts.length + (pref.opts.length % 2) : null;

    const labelRow = (
        <View style={{ paddingVertical: 16, flexDirection: 'row', alignItems: "center" }}>

            <View style={{ flex: 1, marginRight: 10 }}>
                <Text style={[{ color: theme.type[10], fontFamily: theme.font.medium, fontSize: 12, marginBottom: 2 }]}>{pref.title}</Text>
                <Text style={[{ color: theme.type[30], fontFamily: theme.font.regular, fontSize: 10 }]}>{pref.desc}</Text>
            </View>

            {!pref.opts ? (
                <Slider active={active} />
            ) : null}
        </View>
    );

    return (
        <View style={{ opacity: pref.disabled ? 0.5 : 1 }}>
            {pref.opts ? (
                <View style={{ paddingHorizontal: theme.screenHorizPadding }}>
                    {labelRow}
                    <View style={{ flexDirection: "column", flexWrap: "wrap", height: (42 * numVerticalItems / 2) + (6 * 2) /*(height * numVerticalItems / 2) + (padding * 2)*/, backgroundColor: theme.ui[5], borderRadius: 10, padding: 6, marginBottom: 10 }}>
                        {pref.opts.map(option => (
                            <TouchableOpacity
                                key={option.text}
                                onPress={() => state.update(option.val)}
                                style={{ width: "50%", backgroundColor: state.val == option.val ? theme.accent.element.ui : null, borderRadius: 10, paddingHorizontal: 10, height: 42, flexDirection: "row", alignItems: "center" }}>
                                <View style={{ marginRight: 4 }}><RoundIcon name={option.icon} color={state.val == option.val ? theme.accent.element.type : theme.type[10]} size={18} /></View>
                                <Text style={{ color: state.val == option.val ? theme.accent.element.type : theme.type[10], fontSize: 12, fontFamily: theme.font.medium }}>{option.text}</Text>
                            </TouchableOpacity>
                        ))}
                    </View>
                </View>
            ) : (
                <TouchEffect
                    wide
                    onPress={() => state.toggle()}
                    disabled={Boolean(pref.disabled)}>
                    {labelRow}
                </TouchEffect>
            )}
        </View>
    );
}