import React, { useContext } from "react";
import { Text, View } from "react-native";

import themeContext from "../contexts/theme";
import RoundIcon from "./RoundIcon";
import TouchEffect from "./TouchEffect";

export default function HeaderSection({ title, icon, active, canSplit, children, onPress, hiddenBehind, accented, allTouchable }) {
    const theme = useContext(themeContext);

    return (
        <View style={{ marginTop: 10 }}>
            <TouchEffect wide outerStyle={{ backgroundColor: active ? theme.ui[10] : undefined }} disabled={active} onPress={onPress}>
                <View style={{ paddingVertical: 12, flexDirection: "row", alignItems: "center", justifyContent: "space-between" }}>
                    <View style={{ flexShrink: 1 }}>
                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                            <View style={{ width: 40, marginRight: 6, alignItems: "center" }}>
                                <RoundIcon color={accented ? theme.accent.type : theme.type[10]} name={icon} size={24} />
                            </View>
                            <Text style={{ fontFamily: accented ? theme.font.semibold : theme.font.medium, color: accented ? theme.accent.type : theme.type[10], fontSize: 16 }}>{title}</Text>
                            {hiddenBehind ? (
                                <Text style={{ textTransform: "uppercase", color: theme.type[80], fontFamily: theme.font.medium, fontSize: 12, marginLeft: 10 }}>{hiddenBehind}</Text>
                            ) : null}
                        </View>
                        {allTouchable ? children : null}
                    </View>
                    {active ? null : <RoundIcon color={theme.type[50]} name={theme.bigScreen && canSplit ? "left_panel_open" : "keyboard_arrow_right"} size={theme.bigScreen ? 18 : 22} />}
                </View>
            </TouchEffect>
            {!allTouchable ? children : null}
        </View>
    );
}