import React, { useContext, useRef, useMemo } from "react";
import {
    View,
    Animated
} from "react-native";
import themeContext from "../contexts/theme";

export default function Slider({ active, color }) {
    const theme = useContext(themeContext);

    const sliderAnim = useRef(new Animated.Value(active ? 26 : 0)).current;
    useMemo(() => {
        Animated.timing(
            sliderAnim,
            {
                toValue: active ? 20 : 0,
                duration: 150,
                useNativeDriver: false
            }
        ).start();
        //state.index, visibleRoutes.length
    }, [sliderAnim, active]);

    return (
        <View style={{
            width: 50,
            backgroundColor: active ? theme.accent.element.ui : theme.ui[20],
            borderRadius: 20,
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            padding: 6
        }}>
            <Animated.View
                style={{
                    width: 18,
                    height: 18,
                    borderRadius: 9,
                    backgroundColor: active ? theme.accent.element.type : theme.ui[70],
                    marginLeft: sliderAnim
                }}>
            </Animated.View>
        </View>
    );
}