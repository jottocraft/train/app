import React, { createContext, useCallback, useContext, useEffect, useRef, useState } from "react";
import { View } from "react-native";
import { NavigationContainer, createNavigatorFactory, useNavigation, useNavigationBuilder, useNavigationContainerRef, StackRouter } from "@react-navigation/native";
import { NativeStackView } from "@react-navigation/native-stack";

import themeContext from "../contexts/theme";

export const asideContext = createContext(null);

/**
 * This extends NativeStackNavigator to forward all navigations that can't be shown in the
 * primary panel to the aside panel. It should be used for the primary navigator / child of a SplitStack.
 */
function SplitStackNavigator({
    initialRouteName,
    backBehavior,
    children,
    screenOptions,
    ...rest
}) {
    const { asideRef, asideActionQueue } = useContext(asideContext);

    const { state, descriptors, navigation, NavigationContent } =
        useNavigationBuilder(routerOptions => {
            const stackRouter = StackRouter(routerOptions);
            return {
                ...stackRouter,
                getStateForAction(state, action, options) {
                    if (action.type !== "NAVIGATE") return stackRouter.getStateForAction(state, action, options);

                    if (!state.routeNames.includes(action.payload.name) && !action.payload.params?.screen) {
                        if (asideRef?.isReady()) {
                            asideRef.dispatch(action);
                        } else {
                            asideActionQueue.current.push(action);
                        }

                        return state;
                    }

                    return stackRouter.getStateForAction(state, action, options);
                }
            };
        }, {
            initialRouteName,
            backBehavior,
            children,
            screenOptions
        });
    
    return (
        <NavigationContent>
            <NativeStackView
                {...rest}
                state={state}
                navigation={navigation}
                descriptors={descriptors}
            />
        </NavigationContent>
    );
}

export const createSplitStackNavigator = createNavigatorFactory(SplitStackNavigator);

/**
 * Displays two stack navigators side-by-side for a two-pane tablet experience.
 * The main navigator is passed as the child, and the secondary is passed through the aside prop.
 * The main navigator MUST use createSplitStackNavigator for proper two-pane event handling.
 */
export default function SplitStack({ children, aside }) {
    const theme = useContext(themeContext);

    const asideRef = useNavigationContainerRef();
    const asideActionQueue = useRef([]);
    const [asideIsReady, setAsideIsReady] = useState(false);

    useEffect(() => {
        if (!theme.bigScreen) setAsideIsReady(false);
    }, [theme.bigScreen]);

    const navigation = useNavigation();

    const onUnhandledAction = useCallback(action => {
        navigation.dispatch(action);
    }, [navigation]);

    return (
        <asideContext.Provider value={{ asideRef, asideIsReady, asideActionQueue }}>
            <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                    {children}
                </View>
                {theme.bigScreen ? (
                    <View style={{ flex: 1 }}>
                        <NavigationContainer
                            ref={asideRef}
                            independent
                            theme={{
                                dark: theme.dark,
                                colors: {
                                    background: theme.ui[0]
                                }
                            }}
                            onUnhandledAction={onUnhandledAction}
                            onReady={() => {
                                for (const action of asideActionQueue.current) asideRef.dispatch(action);
                                asideActionQueue.current = [];
                                setAsideIsReady(true);
                            }}
                            documentTitle={{ enabled: false }}>
                            {aside}
                        </NavigationContainer>
                    </View>
                ) : null}
            </View>
        </asideContext.Provider>
    );
}