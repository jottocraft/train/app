import React, { useContext, useMemo } from "react";
import { View } from "react-native";
import { useNavigation } from "@react-navigation/native";

import usePref from "../../contexts/prefs";
import HeaderSection from "../HeaderSection";
import Alert, { isAlertImportant } from '../Alert';
import { alertContext } from "../../contexts/timetable";
import { useAsideRoute } from "../Screen";

export default function AlertsSection() {
    const { navigate } = useNavigation();
    const asideRoute = useAsideRoute();
    const alerts = useContext(alertContext);

    const debugAllAlerts = usePref("debugAllAlerts");
    const readAlerts = usePref("readAlerts");

    const sortedAlerts = useMemo(() => {
        var important = [], other = 0;
        if (alerts && alerts.service) {
            important = alerts.service.filter(i => isAlertImportant(i, readAlerts.val));
            other = alerts.service.filter(a => !important.includes(a) && (debugAllAlerts.val ? true : !a.for)).length;
        }

        return { important: important, other: other };
    }, [alerts, readAlerts, debugAllAlerts]);

    if (!sortedAlerts.important.length && !sortedAlerts.other) return null;

    return (
        <HeaderSection active={asideRoute?.name === "Alerts"} accented={sortedAlerts.important.length} allTouchable canSplit hiddenBehind={sortedAlerts.other ? `${sortedAlerts.other} unimportant` : ""} onPress={() => navigate("Alerts")} title="Alerts" icon={sortedAlerts.important.length ? "priority_high" : "campaign"}>
            {sortedAlerts.important.length ? (
                <View style={{ marginTop: 8 }}>
                    {sortedAlerts.important.map(alert => (
                        <Alert {...alert} mini />
                    ))}
                </View>
            ) : null}
        </HeaderSection>
    );
}