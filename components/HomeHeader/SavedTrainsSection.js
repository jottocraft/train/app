import React, { useContext, useMemo } from "react";
import { View } from "react-native";
import { useNavigation } from "@react-navigation/native";

import usePref from "../../contexts/prefs";
import { timetableContext } from "../../contexts/timetable";
import SavedTrain from "../SavedTrain";
import useTrainState from "../../utils/trainState";
import HeaderSection from "../HeaderSection";
import { useAsideRoute } from "../Screen";

export default function SavedTrainsSection() {
    const { navigate } = useNavigation();
    const asideRoute = useAsideRoute();
    const timetable = useContext(timetableContext);

    const trips = timetable.trips;

    const userTrains = usePref("userTrains");

    const savedUserTrains = useMemo(() => {
        if (!trips) return [];

        var active = [];
        Object.keys(userTrains.val).forEach(trainName => {
            var userTrain = userTrains.val[trainName];
            if (!userTrain.saved) return;

            var trainID = Object.keys(trips).find(t => trips[t].name == trainName);
            if (!trainID) return; //Skip trains that can't be found

            var train = trips[trainID];

            active.push(train);
        });

        return active;
    }, [timetable, userTrains]);

    const trainStates = useTrainState(null, ...savedUserTrains);

    const activeUserTrains = useMemo(() => {
        //console.log("USER TRAIN STATES", trainStates);
        const filtered = trainStates.filter(t => t.time);

        return { trains: filtered, extraLen: savedUserTrains.length - filtered.length };
    }, [trainStates, savedUserTrains.length]);

    if (!activeUserTrains.trains.length && !activeUserTrains.extraLen) return null;

    return (
        <HeaderSection active={asideRoute?.name === "MyTrains"} title="Pinned trains" canSplit icon="push_pin" hiddenBehind={activeUserTrains.extraLen ? `${activeUserTrains.extraLen} inactive` : null} onPress={() => navigate("MyTrains")}>
            {/*User Trains*/}
            {activeUserTrains?.trains?.length ? (
                <View style={{ marginBottom: 18 }}>
                    {activeUserTrains?.trains && activeUserTrains.trains.map(nextStop => {
                        const train = trips[nextStop.id];

                        return (
                            <SavedTrain key={nextStop.id} onPress={() => navigate("Train", { id: train.id })} nextStop={nextStop}>{train}</SavedTrain>
                        );
                    })}
                </View>
            ) : null}
        </HeaderSection>
    );
}