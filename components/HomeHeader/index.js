import React, { useCallback, useContext, useMemo, useEffect } from "react";
import { View, Text } from "react-native";
import Animated from "react-native-reanimated";
import { useNavigation } from "@react-navigation/native";

import usePref from "../../contexts/prefs";
import themeContext from "../../contexts/theme";
import { alertContext, streamContext, timetableContext } from "../../contexts/timetable";
import RoundIcon from "../RoundIcon";
import { useBlinkAnimation } from "../../utils/useBlinkAnimation";
import Slider from "../Slider";
import AlertsSection from "./AlertsSection";
import SavedTrainsSection from "./SavedTrainsSection";
import { useTimeFormatter } from "../../utils/time";
import TouchEffect from "../TouchEffect";

export default function HomeHeader() {
    const { navigate } = useNavigation();

    const theme = useContext(themeContext);
    const blinkAnimation = useBlinkAnimation();

    const timetable = useContext(timetableContext);
    const stream = useContext(streamContext);
    const alerts = useContext(alertContext);

    const debugRealtimeDevelopment = usePref("debugRealtimeDevelopment");
    const selectedStream = usePref("selectedStream");
    const dev = usePref("dev");
    const iid = usePref("iid");

    const formatTime = useTimeFormatter();

    const isScheduled = stream.id === "scheduled";
    const isOnline = !isScheduled && stream.hasData;

    const updateStream = useCallback(() => {
        if (stream.id === "real") {
            selectedStream.update("scheduled");
        } else if (stream.id === "scheduled") {
            selectedStream.update("real");
        }
    }, [stream.id, dev, selectedStream]);

    const streamStatus = useMemo(() => {
        if (isScheduled) return {
            status: "Disabled",
            help: "Tap to enable online alerts, delays, and timetable updates"
        };
        if (stream.blink) return {
            status: "Loading...",
            help: "Connecting to the realtime data server..."
        };
        if (alerts.realtime) return {
            status: "Error",
            help: alerts.realtime.text
        };
        return {
            status: "Connected",
            help: dev.val ? (
                `[${debugRealtimeDevelopment.val ? "DEV" : "PROD"}] ${formatTime(alerts.ao)} -> ${formatTime(alerts.ac)} (as ${iid.val?.split("-")[0]})`
            ) : (
                `Last fetched at ${formatTime(alerts.ac)}`
            ),
            warning: alerts.warning
        };
    }, [isScheduled, stream.blink, alerts, stream.id, dev, iid, debugRealtimeDevelopment, formatTime]);

    useEffect(() => {
        if (alerts.timetableUpdate) {
            if (alerts.timetableUpdate.prevName !== timetable?.liveasset?.name) {
                navigate('Notification', {
                    title: "Timetable Updated",
                    description: "Caltrain Live has automatically updated to the " + timetable?.liveasset?.name + " timetable",
                    icon: "schedule"
                });
            } else if (dev.val) {
                navigate('Notification', {
                    title: ".liveasset",
                    description: timetable?.liveasset?.ua + " has been updated to " + timetable?.liveasset?.name + " (" + timetable?.liveasset?.type + "/" + timetable?.liveasset?.ver + ")",
                    icon: "dns"
                });
            }
        }
    }, [!alerts.timetableUpdate, timetable?.liveasset?.name, dev, navigate]);

    return (
        <>
            <TouchEffect
                wide
                onPress={updateStream}
                onLongPress={() => { dev.val && navigate('Debug') }}
            >
                <View style={{ paddingVertical: 10 }}>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <View style={{ marginRight: 6 }}>
                            <Animated.View style={[stream.blink ? blinkAnimation : {}, { overflow: "visible" }]}>
                                <RoundIcon name={stream.icon} size={40} color={theme.streamType[stream.color]} shadowRadius={isOnline ? 5 : 0} />
                            </Animated.View>
                        </View>
                        <View style={{ flexShrink: 1, flexGrow: 1 }}>
                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                                <Text style={{ color: theme.type[10], fontSize: 16, fontFamily: theme.font.semibold, marginRight: 10 }}>Realtime data</Text>
                                <View>
                                    {streamStatus.warning ? (
                                        <Text style={{ textTransform: "uppercase", color: theme.streamType.dataWarning, fontFamily: theme.font.medium, fontSize: 12 }}>Warning</Text>
                                    ) : (
                                        <Text style={{ textTransform: "uppercase", color: theme.streamType[stream.color], fontFamily: theme.font.medium, fontSize: 12 }}>{streamStatus.status}</Text>
                                    )}
                                </View>
                            </View>
                            <Text style={{ color: theme.type[20], fontSize: 12, fontFamily: theme.font.regular, flexWrap: "wrap" }}>{streamStatus.help}</Text>
                            {streamStatus.warning ? (
                                <View style={{ flexDirection: "row", alignItems: "center" }}>
                                    <RoundIcon name="warning" size={12} color={theme.streamType.dataWarning} />
                                    <Text style={{ color: theme.type[30], fontSize: 10, fontFamily: theme.font.regular, flexWrap: "wrap", marginLeft: 2 }}>{streamStatus.warning}</Text>
                                </View>
                            ) : null}
                        </View>
                        <View style={{ marginLeft: 6 }}>
                            <Slider active={!isScheduled} />
                        </View>
                    </View>
                </View>
            </TouchEffect>
            <AlertsSection />
            <SavedTrainsSection />
        </>
    );
}