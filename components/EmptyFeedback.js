import React, { useContext } from "react";
import RoundIcon from "./RoundIcon";
import { View, Text } from "react-native";
import themeContext from "../contexts/theme";

export default function EmptyFeedback({ title, icon = "search_off", children }) {
    const theme = useContext(themeContext);

    return (
        <View style={{ flexGrow: 1, padding: 15, marginVertical: 40 }}>
            <View style={{ flexDirection: "row", alignItems: "center", marginBottom: 5 }}>
                <View style={{ marginRight: 4 }}><RoundIcon name={icon} size={22} color={theme.type[10]} /></View>
                <Text style={{ fontFamily: theme.font.semibold, fontSize: 16, color: theme.type[10] }}>{title}</Text>
            </View>
            <Text style={{ fontFamily: theme.font.regular, fontSize: 14, color: theme.type[20], width: "100%" }}>{children}</Text>
        </View>
    );
}