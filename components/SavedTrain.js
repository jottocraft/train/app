import React, { useContext } from 'react';
import {
    Text,
    View,
} from 'react-native';
import RoundIcon from './RoundIcon';
import themeContext from '../contexts/theme';
import { streamContext } from '../contexts/timetable';
import TouchEffect from './TouchEffect';

export default function SavedTrain({ children, nextStop, onPress, comfy }) {
    const theme = useContext(themeContext);
    const stream = useContext(streamContext);
    const train = children;
    return (
        <View>
            <TouchEffect wide onPress={onPress}>
                <View style={{
                    paddingVertical: comfy ? 14 : 8,
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between"
                }}>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <View style={{ alignItems: "center", width: 40, marginRight: 6 }}>
                            <RoundIcon name={"train"} size={20} color={theme.lineType[train.line]} />
                        </View>
                        <Text style={{ fontFamily: theme.font.regular, fontSize: 14, color: theme.type[10] }}>{train.dir + " " + train.name}</Text>
                    </View>
                    <View style={{ alignItems: "flex-end" }}>
                        {nextStop.delay ? (
                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                                <View style={{ marginRight: 2 }}><RoundIcon name={nextStop.icon} color={nextStop.color} size={16} /></View>
                                <Text style={{ fontFamily: theme.font.medium, fontSize: 12, color: nextStop.color, textAlign: "right" }}>{nextStop.delay}</Text>
                            </View>
                        ) : null}
                        {nextStop.stop ? (
                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: 2 }}>
                                <View style={{ marginRight: 2 }}><RoundIcon name={"flag"} color={theme.type[20]} size={12} /></View>
                                <Text style={{ fontFamily: theme.font.medium, fontSize: 10, color: theme.type[20], textAlign: "right" }}>{nextStop.stop.name}</Text>
                            </View>
                        ) : null}
                    </View>
                </View>
            </TouchEffect>
        </View>
    );
}